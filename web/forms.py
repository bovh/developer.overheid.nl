import logging

from django import forms
from django.conf import settings
from django.core import validators
from django.core.cache import cache
import requests
import yaml

from core import gitlab
from core.models.api import API


logger = logging.getLogger()


class URLField(forms.URLField):
    default_validators = [validators.URLValidator(schemes=["https"])]


class OrganizationField(forms.ChoiceField):
    CACHE_KEY = "organisation_choices"
    CACHE_TTL = 60 * 60

    def __init__(self, **kwargs):
        super().__init__(choices=self._organisation_choices, **kwargs)

    def _organisation_choices(self):
        try:
            return cache.get_or_set(self.CACHE_KEY, self._fetch_organisations, self.CACHE_TTL)
        except Exception:
            return ()

    def _fetch_organisations(self):
        response = requests.get(f"{settings.OO_API_URL}/organisaties", timeout=10)
        organisations = response.json()
        return [(item["systeem_id"], item["naam"]) for item in organisations]

    def has_choices(self):
        return next(iter(self.choices), None) is not None


class ReferenceImplementationField(forms.ModelChoiceField):
    def __init__(self, *args, **kwargs):
        super().__init__(
            queryset=API.objects.filter(is_reference_implementation=True), to_field_name="api_id",
            *args, **kwargs)

    def label_from_instance(self, obj: API):
        return f"{obj.service_name} - {obj.organization.name}"


class AddAPIForm(forms.Form):
    service_name = forms.CharField(min_length=1, max_length=100)
    description = forms.CharField(min_length=1, widget=forms.Textarea())
    organization_ooid = OrganizationField(required=False)
    organization_name = forms.CharField(min_length=1, required=False)
    api_type = forms.ChoiceField(required=False, choices=API.APIType.choices)
    api_authentication = forms.ChoiceField(required=False, choices=API.APIAuthentication.choices)

    production_api_url = URLField(required=True)
    production_specification_url = URLField(required=False)
    production_documentation_url = URLField(required=False)

    acceptance_api_url = URLField(required=False)
    acceptance_specification_url = URLField(required=False)
    acceptance_documentation_url = URLField(required=False)

    demo_api_url = URLField(required=False)
    demo_specification_url = URLField(required=False)
    demo_documentation_url = URLField(required=False)

    contact_email_address = forms.EmailField(required=False)
    contact_phone_number = forms.CharField(required=False)
    contact_url = URLField(required=False)

    reference_implementation = ReferenceImplementationField(required=False, empty_label="")

    government_only = forms.BooleanField(required=False)
    pay_per_use = forms.BooleanField(required=False)
    uptime_guarantee = forms.DecimalField(
        min_value=1, max_value=100, max_digits=3, decimal_places=1, required=False, initial=99.5)
    support_response_time = forms.IntegerField(
        min_value=1, max_value=365, required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.auto_id = "field_%s"

    def create_issue(self):
        title = f"Add a new API: {self.cleaned_data['service_name']}"
        yaml_content = yaml.dump(self.cleaned_data, sort_keys=False)
        content = f"```yaml\n{yaml_content}\n````"

        if settings.DEBUG:
            logger.info("Would have created an issue with:\ntitle: %s\ncontent:\n%s", title, content)
            return

        gitlab.create_issue(title, content, "New API")
