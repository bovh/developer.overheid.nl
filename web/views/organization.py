from django.views.generic import DetailView
from django.urls import reverse

from core.models.organization import Organization

from web.views.generic import BaseView

from web.views.mixins import Breadcrumb


class OrganizationDetailView(DetailView, BaseView):
    template_name = "organization_detail.html"
    limit = 4
    model = Organization
    object: Organization

    def get_title(self):
        return self.object.name

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        organization = self.object

        context["repository_list"] = organization.repository_set.order_by("-last_change").all()[:self.limit]
        context["api_list"] = organization.api_set.order_by("service_name").all()[:self.limit]
        context["repository_list_all_count"] = organization.repository_set.count()
        context["api_list_all_count"] = organization.api_set.count()
        context["limit"] = self.limit

        return context

    def get_breadcrumbs(self):
        return [
            Breadcrumb("Organisaties"),
            Breadcrumb(self.get_title(), reverse(
                "web:organization_detail", kwargs={"slug": self.object.slug}))
        ]
