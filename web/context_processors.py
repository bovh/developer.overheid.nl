from django.conf import settings


def default(_):
    return {
        "GOATCOUNTER_ENABLED": settings.GOATCOUNTER_ENABLED,
    }
