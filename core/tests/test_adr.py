from unittest.mock import patch, MagicMock

from django.test import TestCase

from core.adr import get_apis_to_validate, validate_api
from core.models.api import API, Environment, Organization
from core.validators.base import Validator, ValidatorReport, ValidatorResult

from .models.utils import create_ruleset


class APIsToValidate(TestCase):
    expected = [API.APIType.ODATA, API.APIType.REST_JSON, API.APIType.REST_XML]

    def test_apis_to_validate(self):
        org = Organization.objects.create(name="Test", ooid=1)

        # Create API's for al the types we know
        for _type in API.APIType:
            Environment.objects.create(
                name="production", api_url=f"http://example.com/{_type.name}",
                api=API.objects.create(api_id=f"test-{_type.name}", api_type=_type, organization=org))

        actual = sorted([api.api_type for api, _ in get_apis_to_validate()])

        self.assertListEqual(actual, self.expected)

    def test_testable_types(self):
        self.assertEqual(sorted(API.ADR_TESTABLE_TYPES), self.expected)


class ValidateAPI(TestCase):
    @classmethod
    def setUpTestData(cls):
        org = Organization.objects.create(name="Test", ooid=1)
        cls.api = API.objects.create(api_id="test-api", api_type=API.APIType.REST_JSON, organization=org)
        cls.env = Environment.objects.create(name="production", api_url="http://example.com", api=cls.api)

    @patch.object(Validator, "validate_ruleset")
    def test_validate_api(self, mock_validate_ruleset: MagicMock):
        ruleset = create_ruleset()

        results = [
            ValidatorResult(passed=False, message="Did not pass", details="More details", rule=ruleset.rules.first()),
        ]

        mock_validate_ruleset.return_value = ValidatorReport.objects.create_with_results(results, ruleset=ruleset)
        validate_api(Validator(""), self.api, self.env.api_url)

        self.assertEqual(self.api.validator_reports.count(), 1)

        result: ValidatorResult = results[0]
        self.assertEqual(result.passed, False)
        self.assertEqual(result.message, "Did not pass")
        self.assertEqual(result.details, "More details")
