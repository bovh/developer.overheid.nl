{{/*
Expand the name of the chart.
*/}}
{{- define "don.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "don.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "don.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "don.labels" -}}
helm.sh/chart: {{ include "don.chart" . }}
{{ include "don.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "don.selectorLabels" -}}
app.kubernetes.io/name: {{ include "don.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "don.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "don.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{- define "don.postgresqlFullClusterName" -}}
{{- printf "%s-%s" (include "don.fullname" .) .Values.postgresql.clusterName }}
{{- end }}

{{- define "don.postgresqlSecret" -}}
{{- $username := "don" -}}
{{- printf "%s.%s.credentials.postgresql.acid.zalan.do" $username (include "don.postgresqlFullClusterName" .) }}
{{- end }}

{{- define "don.djangoBaseEnvironment" -}}
- name: SECRET_KEY
  valueFrom:
    secretKeyRef:
      name: {{ include "don.fullname" . }}
      key: secret-key
- name: DB_HOST
  value: {{ include "don.postgresqlFullClusterName" . }}
- name: DB_NAME
  value: {{ .Values.postgresql.databaseName }}
- name: DB_USER
  valueFrom:
    secretKeyRef:
      name: {{ include "don.postgresqlSecret" . }}
      key: username
- name: DB_PASSWORD
  valueFrom:
    secretKeyRef:
      name: {{ include "don.postgresqlSecret" . }}
      key: password
{{- if .Values.config.organizationsAPIURL }}
- name: OO_API_URL
  value: {{ .Values.config.organizationsAPIURL | quote }}
{{- end }}
{{- end }}
