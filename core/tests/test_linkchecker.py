from datetime import datetime
from datetime import timezone
from django.test import TestCase

from core.models.url import URL, URLProbe
from core.linkchecker import find_new_broken_urls


utc = timezone.utc


class Linkchecker(TestCase):
    def test_report_broken_urls(self):
        urls = (
            URL.objects.create(url="http://example.com/0"),
            URL.objects.create(url="http://example.com/1"),
            URL.objects.create(url="http://example.com/2"),
            URL.objects.create(url="http://example.com/3"),
            URL.objects.create(url="http://example.com/4"),
            URL.objects.create(url="http://example.com/5"),
        )

        # Not broken
        URLProbe.objects.create(url=urls[0], timestamp=datetime(2022, 6, 29, 7, 14, 20, tzinfo=utc), status_code=200)
        URLProbe.objects.create(url=urls[0], timestamp=datetime(2022, 6, 29, 8, 14, 20, tzinfo=utc), status_code=200)
        URLProbe.objects.create(url=urls[0], timestamp=datetime(2022, 6, 29, 9, 14, 20, tzinfo=utc), status_code=200)

        # Broken
        URLProbe.objects.create(url=urls[1], timestamp=datetime(2022, 6, 29, 7, 14, 20, tzinfo=utc), status_code=500)
        URLProbe.objects.create(url=urls[1], timestamp=datetime(2022, 6, 29, 8, 14, 20, tzinfo=utc), status_code=500)
        URLProbe.objects.create(url=urls[1], timestamp=datetime(2022, 6, 29, 9, 14, 20, tzinfo=utc), status_code=500)

        # Broken
        URLProbe.objects.create(url=urls[2], timestamp=datetime(2022, 6, 29, 7, 14, 20, tzinfo=utc), error="broken")
        URLProbe.objects.create(url=urls[2], timestamp=datetime(2022, 6, 29, 8, 14, 20, tzinfo=utc), error="broken")
        URLProbe.objects.create(url=urls[2], timestamp=datetime(2022, 6, 29, 9, 14, 20, tzinfo=utc), error="broken")

        # Not broken: need at least 3 failures
        URLProbe.objects.create(url=urls[3], timestamp=datetime(2022, 6, 29, 8, 14, 20, tzinfo=utc), error="broken")
        URLProbe.objects.create(url=urls[3], timestamp=datetime(2022, 6, 29, 9, 14, 20, tzinfo=utc), error="broken")

        # URL 4 is not broken: no probes

        # Not broken: failure threshold not met
        URLProbe.objects.create(url=urls[5], timestamp=datetime(2022, 6, 29, 6, 14, 20, tzinfo=utc), error="broken")
        URLProbe.objects.create(url=urls[5], timestamp=datetime(2022, 6, 29, 7, 14, 20, tzinfo=utc), error="broken")
        URLProbe.objects.create(url=urls[5], timestamp=datetime(2022, 6, 29, 8, 14, 20, tzinfo=utc), status_code=200)
        URLProbe.objects.create(url=urls[5], timestamp=datetime(2022, 6, 29, 9, 14, 20, tzinfo=utc), error="broken")

        broken_urls = find_new_broken_urls(urls, failure_threshold=3)
        self.assertEqual(len(broken_urls), 2)

    def test_all_ready_broken_url(self):
        url = URL.objects.create(url="http://example.com/0")

        URLProbe.objects.create(url=url, timestamp=datetime(2022, 6, 29, 6, 14, 20, tzinfo=utc), error="broken")
        URLProbe.objects.create(url=url, timestamp=datetime(2022, 6, 29, 7, 14, 20, tzinfo=utc), error="broken")
        URLProbe.objects.create(url=url, timestamp=datetime(2022, 6, 29, 8, 14, 20, tzinfo=utc), error="broken")
        URLProbe.objects.create(url=url, timestamp=datetime(2022, 6, 29, 9, 14, 20, tzinfo=utc), error="broken")

        broken_urls = find_new_broken_urls((url,), failure_threshold=3)
        self.assertEqual(len(broken_urls), 0)
