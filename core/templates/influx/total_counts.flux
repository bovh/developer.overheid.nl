import "math"

from(bucket: "{{ bucket }}")
    |> range(start: start, stop: stop)
    |> filter(fn: (r) => r._measurement == "organization" or r._measurement == "api" or r._measurement == "repository")
    |> filter(fn: (r) => r._field == "total_count")
    |> group(columns: ["_measurement", "total_count"])
    |> aggregateWindow(every: window_every, fn: mean, createEmpty: false)
    |> map(fn: (r) => ({r with _value: math.roundtoeven(x: r._value)}))
