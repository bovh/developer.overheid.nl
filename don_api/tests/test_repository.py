from datetime import datetime, UTC
import json

from django.urls import reverse
from rest_framework.compat import SHORT_SEPARATORS
from rest_framework.test import APITestCase

from core.models.organization import Organization
from core.models.repository import Repository, ProgrammingLanguage, RepositoryProgrammingLanguage


class RepositoryTests(APITestCase):
    maxDiff = None

    def test_list(self):
        Repository.objects.create(
            organization=Organization.objects.create(name="Test organization 1", ooid=1),
            source=Repository.Source.GITHUB,
            owner_name="owner-1",
            name="repository-1",
            url="https://example.com/owner-1/repository-1",
            description="Description 1",
            last_change=datetime(2023, 7, 4, 14, 27, 42, tzinfo=UTC),
            stars=12,
            fork_count=34,
            issue_open_count=45,
            merge_request_open_count=56,
            avatar_url="https://example.com/avatar.jpg",
            archived=False,
            last_fetched_at=datetime(2023, 7, 4, 16, 54, 42, tzinfo=UTC),
        )

        repository_2 = Repository.objects.create(
            organization=Organization.objects.create(name="Test organization 2", ooid=2),
            source=Repository.Source.GITLAB,
            owner_name="owner-2",
            name="repository-2",
            url="https://example.com/owner-2/repository-2",
            description="Description 2",
            last_change=datetime(2023, 7, 4, 15, 54, 42, tzinfo=UTC),
            stars=None,
            fork_count=0,
            issue_open_count=None,
            merge_request_open_count=None,
            avatar_url="https://example.com/avatar.jpg",
            archived=False,
            last_fetched_at=datetime(2023, 7, 4, 12, 54, 42, tzinfo=UTC),
        )

        RepositoryProgrammingLanguage.objects.create(
            repository=repository_2,
            programming_language=ProgrammingLanguage.objects.create(name="language-a"),
            usage=42.0,
        )

        response = self.client.get(reverse("repository-list"))
        expected = [
            {
                "source": "gitlab",
                "owner_name": "owner-2",
                "name": "repository-2",
                "organization": {
                    "ooid": 2,
                    "name": "Test organization 2",
                },
                "description": "Description 2",
                "last_change": "2023-07-04T15:54:42Z",
                "url": "https://example.com/owner-2/repository-2",
                "avatar_url": "https://example.com/avatar.jpg",
                "stars": None,
                "fork_count": 0,
                "issue_open_count": None,
                "merge_request_open_count": None,
                "archived": False,
                "programming_languages": ["language-a"],
            },
            {
                "source": "github",
                "owner_name": "owner-1",
                "name": "repository-1",
                "organization": {
                    "ooid": 1,
                    "name": "Test organization 1",
                },
                "description": "Description 1",
                "last_change": "2023-07-04T14:27:42Z",
                "url": "https://example.com/owner-1/repository-1",
                "avatar_url": "https://example.com/avatar.jpg",
                "stars": 12,
                "fork_count": 34,
                "issue_open_count": 45,
                "merge_request_open_count": 56,
                "archived": False,
                "programming_languages": [],
            },
        ]

        self.assertEqual(response.content.decode(), json.dumps(expected, separators=SHORT_SEPARATORS))
