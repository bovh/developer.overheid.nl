import sys

from django.core.management.base import BaseCommand

from core.models.organization import Organization
from core.oo import OrganizationFetchError, OrganizationNotFoundError


class Command(BaseCommand):
    def handle(self, *args, **options):
        has_failures = False

        for ooid, name in Organization.objects.values_list("ooid", "name"):
            self.stdout.write(f"Updating {name} ({ooid})...", ending="")
            try:
                Organization.objects.fetch_and_update(ooid)
                self.stdout.write(" done")
            except OrganizationNotFoundError:
                self.stdout.write(" not found", ending="")
                deleted, _ = Organization.objects.delete_if_not_referenced(ooid=ooid)
                if deleted:
                    self.stdout.write(". Deleted because not used", ending="")
                self.stdout.write()
            except OrganizationFetchError as exception:
                has_failures = True
                self.stdout.write(" error")
                self.stdout.write(f"  Unexpected error: {exception}")

        if has_failures:
            sys.exit(1)
