from django.utils.text import slugify


# Taken from https://code.djangoproject.com/ticket/15307#comment:5
def slugify_max(value, max_length=50):
    slug = slugify(value)
    if len(slug) <= max_length:
        return slug

    trimmed_slug = slug[:max_length].rsplit("-", 1)[0]
    if len(trimmed_slug) <= max_length:
        return trimmed_slug

    # First word is > max_length chars, so we have to break it
    return slug[:max_length]
