#!/usr/bin/env sh
set -e

# Run migrations
/app/manage.py migrate

# Start uWSGI processes
uwsgi /app/uwsgi.ini
