from core.models.api import APIValidatorReport

from .generic import TemplateView


class IndexView(TemplateView):
    template_name = "index.html"
    limit = 10

    def get_breadcrumbs(self):
        return None

    def get_context_data(self, **kwargs):
        api_validator_reports = APIValidatorReport.objects\
            .filter(
                report__ruleset__ruleset_id="core",
                is_latest=True
            )\
            .order_by("-report__rule_passed_count")\
            .select_related("api__organization", "report")[:self.limit]

        return super().get_context_data(api_validator_reports=api_validator_reports, **kwargs)
