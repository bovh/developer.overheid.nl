from unittest import TestCase

from core.models.api import API
from web.templatetags.don_extras import format_api_authentication


class DonExtrasTests(TestCase):
    def test_format_api_authentication(self):
        cases = (
            (API.APIAuthentication.UNKNOWN, "Onbekend"),
            (API.APIAuthentication.NONE, "Geen"),
            (API.APIAuthentication.MUTUAL_TLS, "Tweezijdig TLS"),
            (API.APIAuthentication.API_KEY, "API key"),
            (API.APIAuthentication.IP_ALLOW_LIST, "IP-toegangslijst"),
            (API.APIAuthentication.OAUTH2, "OAuth 2.0"),
        )

        for value, expected in cases:
            self.assertEqual(format_api_authentication(str(value)), expected)

        self.assertEqual(format_api_authentication("something-else"), "Onbekend")
