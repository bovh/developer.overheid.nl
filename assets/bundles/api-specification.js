const spec = JSON.parse(document.getElementById("specification").textContent)
const options = {
    theme: {
        breakpoints: {
            small: "576px",
            medium: "1750px",
        },
        schema: {
            labelsTextSize: "1rem",
            typeNameColor: "red",
        },
        typography: {
            fontFamily: "Source Sans Pro, sans-serif",
            fontSize: "18px",
            optimizeSpeed: false,
            smoothing: "antialiased",
            headings: {
                fontWeight: "700",
                lineHeight: "125%",
                fontFamily: "Source Sans Pro, sans-serif",
            },
            lineHeight: "125%",
            code: {
                wrap: true,
                fontSize: "16px",
            },
            links: {
                color: "#0b71a1",
                hover: "#005282",
            },
        },
        fab: {
            color: "#ffff00",
            backgroundColor: "#0000ff",
        },
        sidebar: {
            width: "300px",
            activeTextColor: "#0d4f71",
            textColor: "#212121",
            arrow: {
                size: "24px",
            },
        },
        colors: {
            primary: {
                main: "#0d4f71",
                contrastText: "#212121",
            },
            http: {
                basic: "#575757",
                delete: "#a52c2d",
                get: "#276501",
                head: "#8a3493",
                link: "#05606b",
                options: "#70550f",
                patch: "#8e4116",
                post: "#0050c7",
            },
            responses: {
                error: {
                    color: "#863232",
                    tabTextColor: "#ff7070",
                },
                success: {
                    color: "#215601",
                    tabTextColor: "#215601",
                },
                info: {
                    color: "#0046ad",
                    tabTextColor: "#0046ad",
                },
                redirect: {
                    color: "#4d4d4d",
                    tabTextColor: "#4d4d4d",
                },
            },
        },
        rightPanel: {
            backgroundColor: "#083044",
            textColor: "#fafafa",
        },
    },
}

Redoc.init(spec, options, document.getElementById("specification-wrapper"))
