const { terminalLog, sizes, config } = require("../support/e2e")

const API_RULES_PATH = '/api-rules'

describe('API rules', () => {
  beforeEach(() => {
    cy.visit(API_RULES_PATH)
  })

  it('should show the page title', () => {
    cy.get('h1').contains('API Design Rules')
  })

  context('a11y', () => {
    sizes.forEach(size => {
      it(`${size.toString().replace(",", "x")}: has no detectable a11y violations on load`, () => {
        if (Cypress._.isArray(size)) {
          cy.viewport(size[0], size[1])
        } else {
          cy.viewport(size)
        }
        cy.visit(API_RULES_PATH)
        cy.screenshot().toMatchImageSnapshot();

        cy.injectAxe()
        // Test the page at initial load
        cy.checkA11y(null, config, terminalLog)
      })
    })
  })

})
