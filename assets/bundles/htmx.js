import "htmx.org"

document.body.addEventListener("htmx:pushedIntoHistory", () => {
    window.goatcounter && window.goatcounter.count({"referrer": "htmx"})
});
