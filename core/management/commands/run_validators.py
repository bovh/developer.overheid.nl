import sys

from django.conf import settings
from django.core.management.base import BaseCommand

from core.models.api import API, APIValidatorReport
from core.adr import get_apis_to_validate, validate_api
from core.validators import validators
from core.validators.base import ValidatorExecutionError


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            "--adr-validator-path", default=settings.ADR_VALIDATOR_PATH)

    def handle(self, *args, **options):
        if self._validate_metadata() or self._validate_adr():
            sys.exit(1)

    def _validate_metadata(self):
        self.stdout.write("Running API metadata validator")

        validator = validators["api-metadata"]
        has_failures = False

        for api in API.objects.all():
            self.stdout.write(f"Validating API: {api}")
            try:
                report = validator.validate_ruleset("metadata", api.api_id)
                APIValidatorReport.objects.create(report=report, api=api)
            except Exception as error:
                has_failures = True
                self.stderr.write(f"Unexptected error: {error}", self.style.ERROR)

        return has_failures

    def _validate_adr(self):
        self.stdout.write("Running ADR validator")

        validator = validators["adr"]
        has_failures = False

        for api, env in get_apis_to_validate():
            self.stdout.write(f"Validating API: {api} ({env.api_url})")
            try:
                validate_api(validator, api, env.api_url)
            except ValidatorExecutionError as error:
                self.stdout.write(f"Validation failed: {error}", self.style.ERROR)
            except Exception as error:
                has_failures = True
                self.stderr.write(f"Unexptected error: {error}", self.style.ERROR)

        return has_failures
