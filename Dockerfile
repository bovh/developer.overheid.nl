# renovate: datasource=pypi depName=uWSGI versioning=pep440
ARG UWSGI_VERSION=2.0.22


FROM python:3.11.5-alpine AS build

RUN addgroup -S -g 1001 don && adduser -S -D -H -G don -u 1001 don

# Allow checking for API's using the PKIoverheid Private Root CA
COPY ./contrib/PKIoverheid-PrivateRootCA-G1.crt /usr/local/share/ca-certificates/
RUN update-ca-certificates

RUN apk add --no-cache gcc musl-dev mailcap libffi-dev libpq-dev
RUN python3 -m venv --upgrade-deps /app/venv && /app/venv/bin/pip3 install wheel

COPY ./docker/uwsgi-buildconf.ini /app/don.ini
ENV UWSGI_PROFILE=/app/don.ini
ARG UWSGI_VERSION
RUN /app/venv/bin/pip3 install uwsgi==${UWSGI_VERSION}

COPY ./requirements.txt /app
RUN /app/venv/bin/pip3 install -r /app/requirements.txt


FROM registry.gitlab.com/commonground/don/adr-validator:0.4.0 AS adr-validator


FROM node:18.18.0 AS build-assets

WORKDIR /app

RUN corepack enable

COPY ./.yarn .yarn
COPY ./package.json ./yarn.lock .
COPY ./assets assets

RUN yarn install --immutable
RUN yarn build


FROM python:3.11.5-alpine

RUN apk add --no-cache git libpq

COPY --from=build /etc/mime.types /etc/passwd /etc/group /etc/
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /app/venv /app/venv
COPY --from=adr-validator /usr/local/bin/adr-validator /usr/local/bin/adr-validator
COPY --from=build-assets /app/assets/dist /app/assets/dist

COPY ./don /app/don
COPY ./core /app/core
COPY ./don_api /app/don_api
COPY ./web /app/web
COPY ./manage.py /app/
COPY ./docker/start.sh ./docker/uwsgi.ini /app/

COPY ./assets/images /app/assets/images
COPY ./assets/vendor /app/assets/vendor

ENV PATH="/app/venv/bin:${PATH}" \
    PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    DON_ENVIRONMENT=production

RUN /app/manage.py collectstatic --no-input

WORKDIR /app
EXPOSE 8000
USER don
CMD ["/app/start.sh"]
