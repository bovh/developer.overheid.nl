from datetime import datetime
import decimal
import math

from django import template
from django.template.context import Context
from django.utils import timezone
from django.utils.html import json_script
from django.utils.timesince import timesince
from django.contrib.humanize.templatetags.humanize import NaturalTimeFormatter

from core.models.api import API
from core.models.repository import Repository
from core.models.validator import ValidatorReport

register = template.Library()


@register.filter
def format_api_authentication(value: str):
    match value:
        case API.APIAuthentication.NONE:
            return "Geen"
        case API.APIAuthentication.MUTUAL_TLS:
            return "Tweezijdig TLS"
        case API.APIAuthentication.API_KEY:
            return "API key"
        case API.APIAuthentication.IP_ALLOW_LIST:
            return "IP-toegangslijst"
        case API.APIAuthentication.OAUTH2:
            return "OAuth 2.0"
        case _:
            return "Onbekend"


@register.filter(is_safe=True)
def graph_data_script(value: dict, name: str):
    return json_script(value, f"graph-data-{name}")


@register.filter
def don_naturaltime(value: datetime):
    """
    The 'naturaltime' filter from django.contrib.humanize won't allows us to change the depth.
    Only supports dates in the future.
    """
    now = timezone.now()
    value = min(value, now)
    delta = now - value

    if delta.days != 0:
        return NaturalTimeFormatter.time_strings["past-day"] % {
            "delta": timesince(value, now, time_strings=NaturalTimeFormatter.past_substrings, depth=1),
        }
    if delta.seconds == 0:
        return NaturalTimeFormatter.time_strings["now"]
    if delta.seconds < 60:
        return NaturalTimeFormatter.time_strings["past-second"] % {"count": delta.seconds}
    if delta.seconds // 60 < 60:
        count = delta.seconds // 60
        return NaturalTimeFormatter.time_strings["past-minute"] % {"count": count}

    count = delta.seconds // 60 // 60
    return NaturalTimeFormatter.time_strings["past-hour"] % {"count": count}


def calculate_stroke_dashoffset(percentage_score: decimal.Decimal):
    percentage = percentage_score / 100

    stroke_width = 4
    circle_size = 35
    size = 1

    scaled_stroke_width = stroke_width / circle_size
    radius = 0.5 * size - 0.5 * scaled_stroke_width
    circumference = 2 * math.pi * radius

    dash_offset = (1 - float(percentage)) * circumference

    return dash_offset


@register.inclusion_tag("partials/score_pie.html")
def score_pie(validator_report: ValidatorReport = None, is_large=False):
    max_score = validator_report.rule_total_count
    percentage_score = (validator_report.rule_passed_count / validator_report.rule_total_count) * 100
    score = validator_report.rule_passed_count

    if percentage_score == 0:
        class_name = "lowest"
    elif percentage_score < 30:
        class_name = "low"
    elif percentage_score < 70:
        class_name = "medium"
    elif percentage_score == 100:
        class_name = "highest"
    else:
        class_name = "high"

    return {
        "ruleset_title": validator_report.ruleset.title,
        "score": score,
        "max_score": max_score,
        "class_name": class_name,
        "is_large": is_large,
        "stroke_dashoffset": calculate_stroke_dashoffset(percentage_score)
    }


@register.inclusion_tag("partials/repository_item.html")
def repository_item(repository: Repository):
    if repository.source == Repository.Source.GITHUB:
        owner_name = repository.owner_name
        stars_url = f"{repository.url}/stargazers" if repository.stars is not None else None
        forks_url = f"{repository.url}/forks"
        issues_url = f"{repository.url}/issues" if repository.issue_open_count is not None else None
        merge_requests_url = f"{repository.url}/pulls" if repository.merge_request_open_count is not None else None
    elif repository.source == Repository.Source.GITLAB:
        owner_name = " / ".join(repository.owner_name.split("/"))
        stars_url = f"{repository.url}/-/starrers" if repository.stars is not None else None
        forks_url = f"{repository.url}/-/forks"
        issues_url = f"{repository.url}/-/issues" if repository.issue_open_count is not None else None
        merge_requests_url = f"{repository.url}/-/merge_requests" \
            if repository.merge_request_open_count is not None else None
    else:
        owner_name = repository.owner_name
        stars_url = None
        forks_url = None
        issues_url = None
        merge_requests_url = None

    return {
        "repository": repository,
        "owner_name": owner_name,
        "stars_url": stars_url,
        "forks_url": forks_url,
        "merge_requests_url": merge_requests_url,
        "issues_url": issues_url,
    }


@register.tag("ifmatchview")
def if_match_view(parser, token):
    args = token.split_contents()[1:]
    if not args:
        raise template.TemplateSyntaxError("'ifmatchview' statement requires at least one argument")

    nodelist = parser.parse(("endifmatchview",))
    parser.delete_first_token()

    return IfMatchViewNode(args, nodelist)


class IfMatchViewNode(template.Node):
    def __init__(self, views_names, nodelist) -> None:
        self.view_names = list(views_names)
        self.nodelist = nodelist

    def render(self, context: Context) -> str:
        if request := context.get("request", None):
            if request.resolver_match and request.resolver_match.view_name in self.view_names:
                return self.nodelist.render(context)

        return ""
