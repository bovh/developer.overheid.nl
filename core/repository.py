from collections import namedtuple
from datetime import timedelta, UTC
from decimal import Decimal
import functools
from typing import Optional

from django.core.exceptions import ImproperlyConfigured
from django.conf import settings
from django.db import transaction
from django.db.models import QuerySet
from django.utils import dateparse, timezone
from github import Github, Auth as GithubAuth, UnknownObjectException, RateLimitExceededException
from gitlab import Gitlab, GitlabGetError
from requests import Session

from core.models.repository import (
    Repository,
    ProgrammingLanguage,
    RepositoryProgrammingLanguage,
    PROGRAMMING_LANGUAGE_USAGE_MIN
)

from core.models.topic import Topic


def repositories_needing_update(interval: timedelta) -> QuerySet[Repository]:
    return Repository.objects \
        .filter(last_fetched_at__lt=timezone.now()-interval) \
        .order_by('last_fetched_at')


class SourceException(Exception):
    pass


class RateLimitException(SourceException):
    pass


_sourceRepository = namedtuple(
    'sourceRepository',
    (
        'description', 'last_change', 'avatar_url', 'archived',
        'star_count', 'fork_count', 'issue_open_count', 'merge_request_open_count',
        'languages', 'topics',
    ),
)


class BaseRepositoryUpdater:
    @transaction.atomic
    def update_repository(self, repository: Repository):
        source = self.fetch(repository.owner_name, repository.name)

        if source is None:
            raise SourceException('Source data not found')

        repository.description = source.description if source.description else ''
        repository.last_change = source.last_change
        repository.avatar_url = source.avatar_url
        repository.archived = source.archived
        repository.stars = source.star_count
        repository.fork_count = source.fork_count
        repository.issue_open_count = source.issue_open_count
        repository.merge_request_open_count = source.merge_request_open_count

        repository.last_fetched_at = timezone.now()
        repository.save()

        repository.programming_languages.clear()
        for name, usage in source.languages.items():
            if usage < PROGRAMMING_LANGUAGE_USAGE_MIN:
                continue

            programming_language, _ = ProgrammingLanguage.objects.get_or_create(name=name)
            RepositoryProgrammingLanguage.objects.update_or_create(
                repository=repository,
                programming_language=programming_language,
                defaults={'usage': usage},
            )

        topics = [
            t for t, _ in (
                Topic.objects.get_or_create(name=topic)
                for topic in source.topics
            )
        ]
        repository.topics.set(topics)

    def fetch(self, owner_name: str, name: str) -> _sourceRepository:
        raise NotImplementedError()


class GitHubRepositoryUpdater(BaseRepositoryUpdater):
    def __init__(self, rest_client: Github, graphql_client: Optional["GraphQL"]):
        self._rest_client: Github = rest_client
        self._graphql_client: GraphQL = graphql_client

        if graphql_client:
            self._pr_open_count = self._pr_open_count_graphql
        else:
            self._pr_open_count = self._pr_open_count_rest

    def fetch(self, owner_name: str, name: str) -> _sourceRepository:
        full_name = f'{owner_name}/{name}'

        try:
            repository = self._rest_client.get_repo(full_name)
            languages = calculate_language_percentages(repository.get_languages())
            pull_request_open_count = self._pr_open_count(repository.owner.login, repository.name)
        except RateLimitExceededException as e:
            raise RateLimitException() from e
        except UnknownObjectException:
            return None

        # The open_issues_count is the combined value of issues and pull requests
        issue_open_count = (
            repository.open_issues_count - pull_request_open_count
            if repository.has_issues else None)

        return _sourceRepository(
            description=repository.description,
            last_change=timezone.make_aware(repository.pushed_at, UTC),
            avatar_url=repository.owner.avatar_url,
            archived=repository.archived,
            star_count=repository.stargazers_count,
            fork_count=repository.forks_count,
            issue_open_count=issue_open_count,
            merge_request_open_count=pull_request_open_count,
            languages=languages,
            topics=repository.raw_data['topics'],
        )

    def _pr_open_count_graphql(self, owner: str, name: str) -> int:
        query = f'''
            query {{
                repository(owner:"{owner}", name:"{name}") {{
                    pullRequests(states:OPEN) {{
                        totalCount
                    }}
                }}
            }}'''

        result = self._graphql_client.execute(query)
        return result['repository']['pullRequests']['totalCount']

    def _pr_open_count_rest(self, owner: str, name: str) -> int:
        result = self._rest_client.search_issues(
            query='', repo=f'{owner}/{name}', type='pr', state='open')
        return result.totalCount


class GitLabRepositoryUpdater(BaseRepositoryUpdater):
    def __init__(self, client: Gitlab):
        self._client: Gitlab = client

    def fetch(self, owner_name: str, name: str) -> _sourceRepository:
        project_id = f'{owner_name}/{name}'

        try:
            project = self._client.projects.get(project_id)
        except GitlabGetError:
            return None

        if project.repository_access_level != "enabled":
            return None

        issue_open_count = project.open_issues_count if project.issues_enabled else None

        merge_request_open_count = (
            len(project.mergerequests.list(state='opened', view='simple'))
            if project.merge_requests_enabled else None)

        return _sourceRepository(
            description=project.description,
            last_change=dateparse.parse_datetime(project.last_activity_at),
            avatar_url=project.avatar_url,
            archived=project.archived,
            star_count=project.star_count,
            fork_count=project.forks_count,
            issue_open_count=issue_open_count,
            merge_request_open_count=merge_request_open_count,
            languages=project.languages(),
            topics=project.topics,
        )


class GraphQL:
    """
    A very very simple GraphQL client
    """

    def __init__(self, url: str, token: str) -> None:
        self._url = url
        self._session = Session()
        self._session.headers['Authorization'] = f'bearer {token}'

    def execute(self, query: str) -> dict:
        json = {
            'query': query.replace('\n', ''),
        }
        response = self._session.post(self._url, json=json)
        if not response.ok:
            raise SourceException(response.text)

        data = response.json()
        if 'errors' in data:
            raise SourceException(data['errors'])

        return data['data']


@functools.cache
def get_repository_updater(source: Repository.Source) -> BaseRepositoryUpdater:
    if source == Repository.Source.GITHUB:
        if token := settings.GITHUB_ACCESS_TOKEN:
            auth = GithubAuth.Token(token)
            graphql_client = GraphQL('https://api.github.com/graphql', token)
        else:
            auth, graphql_client = None, None

        return GitHubRepositoryUpdater(Github(auth=auth), graphql_client)

    if source == Repository.Source.GITLAB:
        return GitLabRepositoryUpdater(create_gitlab_client())

    raise ValueError(source)


def create_gitlab_client() -> Gitlab:
    token = settings.GITLAB['ACCESS_TOKEN']
    if not token:
        raise ImproperlyConfigured('GitLab access token empty')

    return Gitlab(private_token=token)


def calculate_language_percentages(languages: dict) -> dict:
    total_size = functools.reduce(lambda x, y: x+y, languages.values(), 0)

    return {
        name: float(round(Decimal((size/total_size) * 100), 1))
        for name, size in languages.items()
    }
