from unittest.mock import Mock, patch

from django.test.testcases import TestCase
from django.urls import reverse
from requests import Response

from core.models.api import API, Environment, Organization


class APISpecificationTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        api = API.objects.create(
            api_id="test-api",
            organization=Organization.objects.create(name="test-organization", ooid=1),
        )

        Environment.objects.create(api=api, name="production", specification_url="https://example.com")
        Environment.objects.create(api=api, name="acceptance", specification_url="")

    def test_unknown_api(self):
        response = self.client.get(reverse("web:api_specification", args=("does_not_exist", "production")))
        self.assertEqual(response.status_code, 404)

    def test_unknown_environment_type(self):
        response = self.client.get(reverse("web:api_specification", args=("test-api", "something-else")))
        self.assertEqual(response.status_code, 404)

    @patch("requests.get")
    def test_empty_url(self, mock_get: Mock):
        response = self.client.get(reverse("web:api_specification", args=("test-api", "demo")))
        self.assertEqual(response.status_code, 404)
        mock_get.assert_not_called()

    @patch("requests.get")
    @patch("web.views.api.APISpecificationView.CACHE_TTL", 0)
    def test_not_found_on_remote(self, mock_get: Mock):
        return_value = Response()
        return_value.status_code = 404
        mock_get.return_value = return_value

        response = self.client.get(reverse("web:api_specification", args=("test-api", "production")))
        self.assertEqual(response.status_code, 200)
        self.assertIsNone(response.context["specification"])

        mock_get.assert_called_once()

    @patch("requests.get")
    @patch("web.views.api.APISpecificationView.CACHE_TTL", 0)
    def test_not_parsable(self, mock_get: Mock):
        return_value = Response()
        return_value.status_code = 200
        return_value._content = "answer: '"  # noqa: pylint: protected-access
        mock_get.return_value = return_value

        response = self.client.get(reverse("web:api_specification", args=("test-api", "production")))
        self.assertEqual(response.status_code, 200)
        self.assertIsNone(response.context["specification"])
        self.assertContains(response, "Failed to parse specification. Error: found unexpected end of stream")

        mock_get.assert_called_once()

    @patch("requests.get")
    @patch("web.views.api.APISpecificationView.CACHE_TTL", 0)
    def test_ok(self, mock_get: Mock):
        return_value = Response()
        return_value.status_code = 200
        return_value._content = "answer: 42\nok: false"  # noqa: pylint: protected-access
        mock_get.return_value = return_value

        response = self.client.get(reverse("web:api_specification", args=("test-api", "production")))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["specification"], {"answer": 42, "ok": False})
        self.assertIsNone(response.context["specification_error"])

        mock_get.assert_called_once_with("https://example.com", timeout=10, stream=True)
