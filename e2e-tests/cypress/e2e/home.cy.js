const { terminalLog, sizes, baseUrl, config } = require("../support/e2e")

describe('Home', () => {
  beforeEach(() => {
    cy.visit('/')
  })

  it.skip('should have top-10', () => {
    console.log(cy.get('.api-score-list').find('li'))
    cy.get('.api-score-list').find('li').its('length').should('be.within', 1, 10)
  })

  context('a11y', () => {
    sizes.forEach(size => {
      it(`${size.toString().replace(",", "x")}: has no detectable a11y violations on load`, () => {
        if (Cypress._.isArray(size)) {
          cy.viewport(size[0], size[1])
        } else {
          cy.viewport(size)
        }
        cy.visit('/')
        cy.screenshot().toMatchImageSnapshot();

        cy.injectAxe()
        // Test the page at initial load
        cy.checkA11y(null, config, terminalLog)
      })
    })
  })
})
