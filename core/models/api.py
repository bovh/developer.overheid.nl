from typing import NamedTuple

from django.db import models
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _

from . import MAX_ENUM_LENGTH, MAX_TEXT_LENGTH, MAX_URL_LENGTH

from .organization import Organization
from .topic import Topic
from .validator import ForeignValidatorReport, BaseValidatorReport, ValidatorReportsMixin


class API(ValidatorReportsMixin["APIValidatorReport"], models.Model):
    class APIType(models.TextChoices):
        UNKNOWN = 'unknown', 'Onbekend'
        ODATA = 'odata', 'OData'
        REST_JSON = 'rest_json', 'REST/JSON'
        REST_XML = 'rest_xml', 'REST/XML'
        SOAP_XML = 'soap_xml', 'SOAP/XML'
        GRPC = 'grpc', 'gRPC'
        GRAPHQL = 'graphql', 'GraphQL'
        SPARQL = 'sparql', 'SPARQL'
        WFS = 'wfs', 'WFS'
        WMS = 'wms', 'WMS'

    class APIAuthentication(models.TextChoices):
        UNKNOWN = 'unknown', 'Unknown'
        NONE = 'none', 'None'
        MUTUAL_TLS = 'mutual_tls', 'Mutal TLS'
        API_KEY = 'api_key', 'API key'
        IP_ALLOW_LIST = 'ip_allow_list', 'IP allow list'
        OAUTH2 = "oauth2", "OAuth 2.0"

    api_id = models.CharField(max_length=MAX_TEXT_LENGTH, unique=True)
    description = models.TextField()
    organization = models.ForeignKey(
        Organization, verbose_name=_("organization"), on_delete=models.PROTECT)
    service_name = models.CharField(max_length=MAX_TEXT_LENGTH)
    api_type = models.CharField(
        max_length=MAX_ENUM_LENGTH,
        choices=APIType.choices,
        default=APIType.UNKNOWN
    )
    api_authentication = models.CharField(
        max_length=MAX_ENUM_LENGTH,
        choices=APIAuthentication.choices,
        default=APIAuthentication.UNKNOWN
    )
    is_reference_implementation = models.BooleanField(default=False)

    contact_email = models.CharField(max_length=MAX_TEXT_LENGTH, blank=True)
    contact_phone = models.CharField(max_length=MAX_TEXT_LENGTH, blank=True)
    contact_url = models.URLField(max_length=MAX_URL_LENGTH, blank=True)

    terms_government_only = models.BooleanField(null=True)
    terms_pay_per_use = models.BooleanField(null=True)
    terms_uptime_guarantee = models.DecimalField(
        null=True,
        decimal_places=6,
        max_digits=8,
    )
    terms_support_response_time = models.PositiveIntegerField(
        help_text='Measured in number of workdays',
        null=True,
    )

    forum_vendor = models.CharField(max_length=MAX_ENUM_LENGTH, blank=True)
    forum_url = models.URLField(max_length=MAX_URL_LENGTH, blank=True)

    badges = models.ManyToManyField(
        'Badge',
        through='APIBadge',
        related_name='apis',
    )
    referenced_apis = models.ManyToManyField(
        'self',
        through='Relation',
        symmetrical=False,
        related_name='referenced_by_apis',
    )
    topics = models.ManyToManyField(Topic)

    def __str__(self):
        return self.api_id

    def get_absolute_url(self):
        return reverse("web:api_detail", kwargs={"api_id": self.api_id})

    def get_production_environment(self):
        return self.environments.filter(name=Environment.EnvironmentType.PRODUCTION).first()

    ADR_TESTABLE_TYPES = (APIType.REST_JSON.ODATA, APIType.REST_JSON, APIType.REST_XML)
    """API's of these types can be tested on API Design Rules"""

    def is_adr_testable(self):
        return self.api_type in self.ADR_TESTABLE_TYPES

    class BasicScore(NamedTuple):
        has_documentation: bool
        has_specification: bool
        has_contact_details: bool
        provides_sla: bool

    @cached_property
    def basic_api_score(self) -> "BasicScore":
        production_environment = self.get_production_environment()

        return self.BasicScore(
            has_documentation=(production_environment and production_environment.documentation_url != ""),
            has_specification=(production_environment and production_environment.specification_url != ""),
            has_contact_details=(self.contact_email != "" or self.contact_phone != "" or self.contact_url != ""),
            provides_sla=(self.terms_support_response_time is not None and self.terms_uptime_guarantee >= 0.9)
        )

    class Meta:
        verbose_name = _("API")
        verbose_name_plural = _("APIs")


class Environment(models.Model):
    class EnvironmentType(models.TextChoices):
        PRODUCTION = 'production', 'Production'
        ACCEPTANCE = 'acceptance', 'Acceptance'
        TEST = 'test', 'Test'
        DEMO = 'demo', 'Demo'

    name = models.CharField(max_length=MAX_ENUM_LENGTH, choices=EnvironmentType.choices)
    api_url = models.URLField(max_length=MAX_URL_LENGTH)
    specification_url = models.URLField(max_length=MAX_URL_LENGTH, blank=True)
    documentation_url = models.URLField(max_length=MAX_URL_LENGTH, blank=True)

    api = models.ForeignKey(
        API,
        to_field='api_id',
        on_delete=models.CASCADE,
        related_name='environments',
    )

    def __str__(self):
        return f"{self.name} - {self.api_url}"


class Relation(models.Model):
    TYPE_REFERENCE_IMPLEMENTATION = "reference-implementation"

    name = models.CharField(max_length=MAX_TEXT_LENGTH, blank=True)
    from_api = models.ForeignKey(
        API,
        to_field='api_id',
        on_delete=models.CASCADE,
        related_name='relations_from',
    )
    to_api = models.ForeignKey(
        API,
        to_field='api_id',
        on_delete=models.PROTECT,
        related_name='relations_to',
    )

    def __str__(self):
        return f'{self.from_api_id} {self.name} {self.to_api_id}'


class APIValidatorReport(BaseValidatorReport):
    api = ForeignValidatorReport(API)
