from datetime import datetime, timezone
from unittest.mock import MagicMock, patch

from django.test import TestCase, SimpleTestCase
from gitlab import Gitlab, GitlabGetError

from core.models.organization import Organization
from core.models.repository import Repository, ProgrammingLanguage, RepositoryProgrammingLanguage
from core.repository import (
    BaseRepositoryUpdater, GitLabRepositoryUpdater, _sourceRepository, calculate_language_percentages
)


class MockUpdater(BaseRepositoryUpdater):
    def __init__(self, source) -> None:
        self._source = source

    def fetch(self, owner_name: str, name: str):
        return self._source


class TestUpdateRepository(TestCase):
    def test_programming_language(self):
        repository = Repository.objects.create(
            organization=Organization.objects.create(name="Test organization", ooid=1),
            source=Repository.Source.GITLAB,
            owner_name="test-owner",
            name="test-repository",
            last_change=datetime(2022, 6, 20, 12, 39, 40, tzinfo=timezone.utc),
            last_fetched_at=datetime(2022, 6, 20, 12, 39, 40, tzinfo=timezone.utc),
        )

        RepositoryProgrammingLanguage.objects.create(
            repository=repository,
            programming_language=ProgrammingLanguage.objects.create(name="Python"),
            usage=100,
        )
        RepositoryProgrammingLanguage.objects.create(
            repository=repository,
            programming_language=ProgrammingLanguage.objects.create(name="Test"),
            usage=42,
        )

        source = _sourceRepository(
            description="Updated repository",
            star_count=12,
            fork_count=34,
            issue_open_count=56,
            merge_request_open_count=78,
            avatar_url="http://example.com/avatar.png",
            archived=False,
            last_change=datetime(2022, 6, 20, 12, 39, 42, tzinfo=timezone.utc),
            languages={"Python": 42, "HTML": 32},
            topics=(),
        )
        updater = MockUpdater(source)
        updater.update_repository(repository)

        languages = repository.repositoryprogramminglanguage_set.all()
        self.assertEqual(len(languages), 2)

        self.assertEqual(languages[0].programming_language.name, "Python")
        self.assertEqual(languages[0].usage, 42)

        self.assertEqual(languages[1].programming_language.name, "HTML")
        self.assertEqual(languages[1].usage, 32)


class TestLanguages(SimpleTestCase):
    def test_calculate_percentages(self):
        tests = (
            (
                {
                    "Python": 4242,
                },
                {
                    "Python": 100.0,
                },
            ),
            (
                {
                    "Python": 4242,
                    "JavaScript": 0,
                },
                {
                    "Python": 100.0,
                    "JavaScript": 0,
                },
            ),
            (
                {
                    "Vim script": 15650,
                    "Shell": 571,
                },
                {
                    "Vim script": 96.5,
                    "Shell": 3.5,
                },
            ),
            (
                {
                    "TypeScript": 1760784,
                    "JavaScript": 1225036,
                    "CSS": 3130,
                    "Dockerfile": 1983,
                    "Makefile": 1610,
                    "HTML": 1569,
                },
                {
                    "TypeScript": 58.8,
                    "JavaScript": 40.9,
                    "CSS": 0.1,
                    "Dockerfile": 0.1,
                    "Makefile": 0.1,
                    "HTML": 0.1,
                },
            ),
            (
                {
                    "Jupyter Notebook": 515114,
                    "Python": 15998,
                    "HTML": 9622,
                    "Dockerfile": 423,
                    "Shell": 253,
                },
                {
                    "Jupyter Notebook": 95.1,
                    "Python": 3.0,
                    "HTML": 1.8,
                    "Dockerfile": 0.1,
                    "Shell": 0.0,
                },
            ),
        )

        for languages, expected in tests:
            with self.subTest():
                actual = calculate_language_percentages(languages)
                self.assertEqual(actual, expected)


@patch.object(Gitlab, "http_get")
class TestGitLabUpdater(SimpleTestCase):

    @patch.object(Gitlab, "http_list")
    def test_project(self, http_list: MagicMock, http_get: MagicMock):
        http_get.side_effect = (
            {
                "id": 42,
                "description": "A test project",
                "name": "Test",
                "name_with_namespace": "Owner / Group / Test",
                "path": "test",
                "path_with_namespace": "owner/group/test",
                "created_at": "2019-01-24T21:55:34.320Z",
                "default_branch": "main",
                "tag_list": [],
                "topics": ["topic-a", "topic-b"],
                "forks_count": 12,
                "avatar_url": "http://example.com/avatar.png",
                "star_count": 34,
                "last_activity_at": "2023-02-01T10:50:34.000Z",
                "archived": False,
                "issues_enabled": True,
                "merge_requests_enabled": True,
                "repository_access_level": "enabled",
                "open_issues_count": 1
            },
            {
                "Python": 45.5,
                "HTML": 54.5,
            },
        )

        http_list.side_effect = (
            [{"id": 1}, {"id": 2}],  # Merge requests
        )

        expected = _sourceRepository(
            description="A test project",
            star_count=34,
            fork_count=12,
            issue_open_count=1,
            merge_request_open_count=2,
            avatar_url="http://example.com/avatar.png",
            archived=False,
            last_change=datetime(2023, 2, 1, 10, 50, 34, tzinfo=timezone.utc),
            languages={"Python": 45.5, "HTML": 54.5},
            topics=["topic-a", "topic-b"],
        )
        actual = GitLabRepositoryUpdater(Gitlab()).fetch("owner/group", "test")
        self.assertTupleEqual(expected, actual)

    def test_project_without_issues_and_merge_requests(self, http_get: MagicMock):
        http_get.side_effect = (
            {
                "id": 42,
                "description": "A test project",
                "name": "Test",
                "name_with_namespace": "Owner / Group / Test",
                "path": "test",
                "path_with_namespace": "owner/group/test",
                "created_at": "2019-01-24T21:55:34.320Z",
                "default_branch": "main",
                "tag_list": [],
                "topics": [],
                "forks_count": 12,
                "avatar_url": "http://example.com/avatar.png",
                "star_count": 34,
                "last_activity_at": "2023-02-01T10:50:34.000Z",
                "archived": False,
                "issues_enabled": False,
                "merge_requests_enabled": False,
                "repository_access_level": "enabled",
            },
            {},  # Languages
        )

        expected = _sourceRepository(
            description="A test project",
            star_count=34,
            fork_count=12,
            issue_open_count=None,
            merge_request_open_count=None,
            avatar_url="http://example.com/avatar.png",
            archived=False,
            last_change=datetime(2023, 2, 1, 10, 50, 34, tzinfo=timezone.utc),
            languages={},
            topics=[],
        )
        actual = GitLabRepositoryUpdater(Gitlab()).fetch("owner/group", "test")
        self.assertTupleEqual(expected, actual)

    def test_project_without_repository(self, http_get: MagicMock):
        http_get.side_effect = (
            {
                "id": 42,
                "description": "A test project",
                "name": "Test",
                "name_with_namespace": "Owner / Group / Test",
                "path": "test",
                "path_with_namespace": "owner/group/test",
                "created_at": "2019-01-24T21:55:34.320Z",
                # default_branch is absent
                "tag_list": [],
                "topics": [],
                # forks_count is absent
                "avatar_url": "http://example.com/avatar.png",
                "star_count": 34,
                "last_activity_at": "2023-02-01T10:50:34.000Z",
                "archived": False,
                "issues_enabled": False,
                "merge_requests_enabled": False,
                "repository_access_level": "private",
            },
        )

        actual = GitLabRepositoryUpdater(Gitlab()).fetch("owner/group", "test")
        self.assertIsNone(actual)

    def test_project_not_found(self, http_get: MagicMock):
        http_get.side_effect = GitlabGetError("Not found")

        actual = GitLabRepositoryUpdater(Gitlab()).fetch("owner/group", "test")
        self.assertIsNone(actual)
