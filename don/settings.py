import os
from pathlib import Path


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = Path(__file__).resolve().parent.parent

DEBUG_DEFAULT = 'True'
SECRET_KEY_DEFAULT = '0)l39j8gmr17ygx@7oia_x#x$o@t4qh&dx^$o4j+fjfi-3-0=7'  # noqa

DON_PRODUCTION = os.getenv("DON_ENVIRONMENT") == "production"

if DON_PRODUCTION:
    DEBUG_DEFAULT = 'False'
    SECRET_KEY_DEFAULT = None
    USE_X_FORWARDED_HOST = True
    USE_X_FORWARDED_PORT = True
    SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

DEBUG = os.getenv('DEBUG', DEBUG_DEFAULT) == 'True'
SECRET_KEY = os.getenv('SECRET_KEY', SECRET_KEY_DEFAULT)

if os.getenv('ALLOWED_HOST'):
    ALLOWED_HOSTS = [os.getenv('ALLOWED_HOST')]

OO_API_URL = os.environ.get('OO_API_URL', 'https://developer.overheid.nl/oo/api/v0')

GITLAB = {
    'ACCESS_TOKEN': os.environ.get('GITLAB_ACCESS_TOKEN'),
    'PROJECT_ID': os.environ.get('GITLAB_PROJECT_ID'),
    'URL': os.environ.get('GITLAB_URL'),
}
GITHUB_ACCESS_TOKEN = os.environ.get('GITHUB_ACCESS_TOKEN', '')

METRICS_INFLUXDB = {
    'URL':  os.environ.get('INFLUXDB_URL', "http://localhost:8086"),
    'TOKEN': os.environ.get('INFLUXDB_TOKEN', 'don'),
    'ORGANIZATION': 'DON',
    'BUCKET': 'don',
}

LINKCHECKER_FAILURE_THRESHOLD = os.environ.get('LINKCHECKER_FAILURE_THRESHOLD', 24)

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.humanize',
    'django.contrib.staticfiles',
    'rest_framework',
    'core.apps.CoreConfig',
    'web',
    'don_api',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'don.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'web.context_processors.default',
            ],
        },
    },
]

WSGI_APPLICATION = 'don.wsgi.application'

REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'don_api.pagination.LinkHeaderPagination',
    'PAGE_SIZE': 10,
}

# Application behaviour
APPEND_SLASH = False


# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'HOST': os.getenv('DB_HOST', 'localhost'),
        'USER': os.getenv('DB_USER', 'don'),
        'PASSWORD': os.getenv('DB_PASSWORD', 'don'),
        'NAME': os.getenv('DB_NAME', 'don'),
        'CONN_MAX_AGE': None,
        'CONN_HEALTH_CHECKS': True,
        'OPTIONS': {
            'application_name': "don_api",
            'keepalives': "1",
            'keepalives_idle': "120",
            'keepalives_interval': "20",
        }
    }
}

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

if DON_PRODUCTION:
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'handlers': {
            'console': {
                "level": "INFO",
                'class': 'logging.StreamHandler',
            },
        },
        'loggers': {
            'django': {
                'handlers': ['console'],
                'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
            },
        }
    }

# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = 'en-gb'

TIME_ZONE = 'Europe/Amsterdam'

USE_I18N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/

STATIC_URL = "/static/"
STATIC_ROOT = BASE_DIR / "static"
STATICFILES_DIRS = (
    BASE_DIR / "assets",
)
if DON_PRODUCTION:
    STORAGES = {
        "staticfiles": {
            "BACKEND": "django.contrib.staticfiles.storage.ManifestStaticFilesStorage",
        },
    }

if DEBUG:
    INSTALLED_APPS += ["debug_toolbar"]
    MIDDLEWARE += ["debug_toolbar.middleware.DebugToolbarMiddleware"]
    INTERNAL_IPS = ("127.0.0.1",)

GOATCOUNTER_ENABLED = os.getenv('GOATCOUNTER_ENABLED') == "true"

# Design Rule settings
ADR_VALIDATOR_PATH = os.getenv('ADR_VALIDATOR_PATH', "/usr/local/bin/adr-validator")

REPOSITORY_UPDATE_INTERVAL = os.getenv('REPOSITORY_UPDATE_INTERVAL', '0 days 06:00:00')
