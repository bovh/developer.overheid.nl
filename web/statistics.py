from collections import defaultdict

from influxdb_client.client.flux_table import FluxRecord, TableList

from core.metrics import new_influxdb_client, execute_query


influx_query_api = None


def get_statistics(start_days, window_every):
    global influx_query_api
    if influx_query_api is None:
        influx_client = new_influxdb_client()
        influx_query_api = influx_client.query_api()

    total_counts = execute_query("total_counts", influx_query_api, start_days, window_every)
    design_rules_last = execute_query(
        "design_rules_last", influx_query_api, start_days, window_every)
    design_rules_api_successes_last = execute_query(
        "design_rules_api_successes_last", influx_query_api, start_days, window_every)

    return total_counts, design_rules_last, design_rules_api_successes_last


def serialize_design_rules_api_successes(tables: TableList) -> dict:
    successes_last = defaultdict(dict)
    output = defaultdict(dict)

    for table in tables:
        record: FluxRecord
        for record in table.records:
            successes_last[record["version"]][record["successes"]] = int(record.get_value())

    for version, counts in successes_last.items():
        design_rule_count = len(counts) - 1
        for successes, count in counts.items():
            label = f"{successes}/{design_rule_count}"
            output[version][label] = count

    return output
