from typing import Generic, TypeVar

from django.core.exceptions import ValidationError, ImproperlyConfigured
from django.db import models, transaction
from django.db.models.base import ModelBase
from django.db.models.fields.related import RelatedField
from django.db.models.options import Options


class Validator(models.Model):
    validator_id = models.CharField(max_length=50)
    name = models.CharField(max_length=50)
    description = models.TextField()
    version = models.CharField(max_length=16)
    documentation_url = models.URLField(null=True)

    rulesets: models.QuerySet["ValidatorRuleset"]


class ValidatorRuleset(models.Model):
    ruleset_id = models.CharField(max_length=50)
    title = models.CharField(max_length=100)
    description = models.TextField()
    version = models.CharField(max_length=16)
    documentation_url = models.URLField(null=True)

    validator = models.ForeignKey(Validator, on_delete=models.RESTRICT, related_name="rulesets")

    rules: models.QuerySet["ValidatorRule"]
    reports: models.QuerySet["ValidatorReport"]

    class Meta:
        constraints = (
            models.UniqueConstraint(fields=("ruleset_id", "validator"), name="unique_ruleset_per_validator"),
        )


class ValidatorRule(models.Model):
    rule_id = models.CharField(max_length=50)
    title = models.CharField(max_length=100)
    description = models.TextField()
    documentation_url = models.URLField(null=True)

    ruleset = models.ForeignKey(ValidatorRuleset, on_delete=models.CASCADE, related_name="rules")

    results: models.QuerySet["ValidatorResult"]

    class Meta:
        constraints = (
            models.UniqueConstraint(fields=("rule_id", "ruleset"), name="unique_rule_per_ruleset"),
        )


class ValidatorReportManager(models.Manager):
    def create_with_results(self, results: list["ValidatorResult"], **kwargs) -> "ValidatorReport":
        if not results:
            raise ValidationError("Can't save report without results")

        with transaction.atomic(using=self.db):
            obj: ValidatorReport = super().create(**kwargs)
            for result in results:
                result.report = obj
                result.save(using=self.db)

            obj.calculate_counts(using=self.db)

        return obj


class ValidatorReport(models.Model):
    uri = models.CharField(max_length=200)
    generated_at = models.DateTimeField(auto_now_add=True)
    rule_passed_count = models.PositiveIntegerField(default=0)
    rule_total_count = models.PositiveIntegerField(default=0)

    ruleset = models.ForeignKey(ValidatorRuleset, on_delete=models.CASCADE, related_name="reports")

    results: models.QuerySet["ValidatorResult"]

    objects: ValidatorReportManager = ValidatorReportManager()

    def calculate_counts(self, using=None):
        results = self.results.all()

        passed_count = 0
        for result in results:
            if result.passed:
                passed_count += 1

        self.rule_passed_count = passed_count
        self.rule_total_count = len(results)

        self.save(using=using)


class ValidatorResult(models.Model):
    passed = models.BooleanField()
    message = models.CharField(max_length=500, blank=True)
    details = models.TextField(blank=True)

    report = models.ForeignKey(ValidatorReport, on_delete=models.CASCADE, related_name="results")
    rule = models.ForeignKey(ValidatorRule, on_delete=models.CASCADE, related_name="results")


class ForeignValidatorReport(models.ForeignKey):
    def __init__(self, *args, **kwargs):
        kwargs["on_delete"] = models.CASCADE
        kwargs["related_name"] = "validator_reports"
        super().__init__(*args, **kwargs)


class _BaseValidatorReportBase(ModelBase):
    _meta: Options

    def _prepare(cls: "BaseValidatorReport"):
        for field in cls._meta.get_fields():
            if not field.is_relation or field.name == "report":  # keep in sync with BaseValidatorReport
                continue

            if field.many_to_one:
                cls.parent_relation_field = field
                break

        if cls.parent_relation_field is None:
            raise ImproperlyConfigured(f"{cls.__name__} missing ForeignKey field")

        super()._prepare()


class BaseValidatorReport(models.Model, metaclass=_BaseValidatorReportBase):
    parent_relation_field: RelatedField = None

    report = models.OneToOneField(
        ValidatorReport, on_delete=models.CASCADE, primary_key=True, related_name="+", db_column="validator_report_id")
    is_latest = models.BooleanField(default=False)

    class Meta:
        abstract = True

    def save(self, *args, using=None, **kwargs):
        with transaction.atomic(using=using):
            params = {
                self.parent_relation_field.name: getattr(self, self.parent_relation_field.name),
                "report__ruleset": self.report.ruleset,
                "is_latest": True,
            }
            self._meta.model.objects.filter(**params).update(is_latest=False)

            self.is_latest = True
            super().save(*args, using=using, **kwargs)

    def delete(self, *args, **kwargs):
        return self.report.delete(*args, **kwargs)


_T = TypeVar("_T", bound=models.Model)


class ValidatorReportsMixin(Generic[_T]):
    validator_reports: models.QuerySet[_T]  # keep in sync with ForeignValidatorReport

    @property
    def latest_validator_reports(self) -> models.QuerySet[_T]:
        return self.validator_reports.filter(is_latest=True)\
            .select_related("report")\
            .order_by("report__ruleset__title")
