from .api import APIMetadataValidator, ADRValidator
from .base import ValidatorRegistry


_settings = {
    "api-metadata": APIMetadataValidator,
    "adr": ADRValidator,
}

validators = ValidatorRegistry(_settings)
