# Contributing

## Setup your development environment

### Requirements

* [Python 3.11](https://www.python.org/downloads/)
* [Node.js LTS](https://nodejs.org/en/download/)
* [Docker](https://docs.docker.com/get-docker/)
* [Docker Compose](https://docs.docker.com/compose/install)
* [PostgreSQL 14](https://www.postgresql.org/download/)

### Django

First, start a database for local development:

```sh
docker-compose -f docker-compose.dev.yml up
```

Make sure to install the Postgresql client libraries when you are not on Windows to be able to install `psycopg2`. Depending on the environment you have to do the following.

On a Linux machine install postgresql client libs using the package manager. For example on Ubuntu/Debian execute:

```sh
sudo apt install libpq-dev
```

On Mac OS install postgresql, for example with Homebrew:

```sh
brew install postgresql
```

Next, set up a Python virtual environment and install the dependencies.

```sh
python3 -m venv .venv
source .venv/bin/activate

# pip-tools is used to manage the dependencies
pip install pip-tools

# Install dependencies
pip-sync requirements-dev.txt

# Run the migrations
python manage.py migrate

# Syncs the API JSON files to your database
# This is not necessary when using test data from the above step
python manage.py sync_content --repository https://gitlab.com/commonground/don/don-content

# Run the Django API
python manage.py runserver
```

#### Django jobs

To create dummy data for the statistics page, run:

```sh
python manage.py generate_dummy_data
```

### Migrations

To run all migrations:

```sh
python manage.py migrate
```

To go back to a specific step:

```sh
python manage.py migrate core 0044
```

### Run validator and generate reports

```sh
python manage.py run_validators --adr-validator-path=../adr-validator/
```

### Build assets

```sh
corepack enable
yarn
yarn build
```

## Running with minikube

Run this project with minikube if you want to test changes to the Helm charts or Dockerfiles locally.

### Requiremens

* [kubectl](https://kubernetes.io/docs/tasks/tools/)
* [Minikube 1.24+](https://minikube.sigs.k8s.io/docs/start/)
* [Helm 3](https://helm.sh/docs/intro/install/)

### Create Kubernetes cluster

```sh
# Start minikube
minikube start --cpus 4 --memory 8g --disk-size 32g --addons=ingress-dns

# On macOS
# This allows us to use the .minkukbe TLD
sudo mkdir -p /etc/resolver
sudo tee /etc/resolver/minikube <<EOF
nameserver $(minikube ip)
search_order 1
timeout 5
EOF
```

### Install system wide components

```sh
# Add the required Helm repositories
helm repo add traefik https://helm.traefik.io/traefik
helm repo add postgres-operator https://opensource.zalando.com/postgres-operator/charts/postgres-operator

# Create dedicated namespace
kubectl create namespace minikube-system

# Install Traefik
helm upgrade --install --namespace minikube-system --values helm/traefik-values-minikube.yaml traefik traefik/traefik

# Install Postgres Operator
helm upgrade --install --namespace minikube-system postgres-operator postgres-operator/postgres-operator
```

### Install the DON Helm charts

```sh
# Create dedicated namespace
kubectl create namespace don

# Install the InfluxDB chart for storing the metrics
helm install --namespace don influxdb helm/influxdb

# Activate the minkune Docker environment
eval $(minikube docker-env)

# Build Docker images
docker-compose build

# Install the DON chart
helm install --namespace don don helm/don
```

The website is accessible via http://don.minkube.

## Adding and updating dependencies

### Python

First, make sure your virtual environment is active.

#### New dependency

To include a new module dependency, add the module name to `requirements.in` (for production dependencies) or `requirements-dev.in` (for development and testing dependencies). Then update your dependencies.

#### Update dependencies

To update all dependencies to their newest versions, run

```sh
pip-compile requirements.in --generate-hashes
pip-compile requirements-dev.in --generate-hashes

pip-sync requirements-dev.txt
```

__*NB:*__ The order of the `pip-compile`'s is important!

If you updated your git repository and the new version has changes in the requirements files, also run the `pip-sync` command to synchronize your environment with the requirements files.

> __*Note*__: `pip-sync` will synchronize your environment with the requirements files, that means it will also delete any modules that are not listed in the requirements files. If you do not want that, run `pip install -r requirements-dev.txt` instead of `pip-sync`.

## Conventions for commit messages

### Conventional commits

We follow the [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) specification. This convention requires you to add a type and an optional scope to your commit message. The scope is based on the applications in the repository. If you are not sure which scope to use please leave the scope blank.

The type must be one of the following:

* **build**: Changes that affect the build system or external dependencies
* **ci**: Changes to our CI configuration files and scripts
* **docs**: Documentation only changes
* **feat**: A new feature
* **fix**: A bug fix
* **refactor**: A code change that neither fixes a bug nor adds a feature
* **revert**: Changes that revert other changes
* **style**: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
* **test**: Adding missing tests or correcting existing tests
* **content**: Changing copy on existing content pages

The available scopes are:

* helm

### Issue number prefix

For branches that are linked to a Gitlab issue, the commit message should also be prefixed by the issue number.  The issue number comes after the conventional commit part.

Example: `feat(ui): #235 replace add buttons with links on small devices`
