from core.validators import validators

from .generic import TemplateView


class APIDesignRulesView(TemplateView):
    title = "API Design Rules"
    template_name = "content/api_design_rules.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        validator = validators["adr"]
        core_ruleset = validator.model.rulesets.get(ruleset_id="core")
        context["api_design_rules"] = core_ruleset.rules

        return context
