from django.conf import settings
from django.urls import path
from django.views import defaults as default_views
from django.views.generic.base import RedirectView

from .views.api import (
    APIListView, APIDetailView, APISpecificationView, APIAddView, APIAddSuccessView, APIValidatorReportsView
)
from .views.content import APIDesignRulesView
from .views.contact import ContactView, ContactSuccessView
from .views.generic import TemplateView
from .views.index import IndexView
from .views.organization import OrganizationDetailView
from .views.repository import RepositoryListView
from .views.statistics import StatisticsView


app_name = "web"


urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("apis", APIListView.as_view(), name="api_list"),
    path("apis/toevoegen", APIAddView.as_view(), name="api_add"),
    path("apis/toevoegen/succes", APIAddSuccessView.as_view(), name="api_add_success"),
    path("apis/<slug:api_id>", APIDetailView.as_view(), name="api_detail"),
    path("apis/<slug:api_id>/score-details", APIValidatorReportsView.as_view(), name="api_validator_reports"),
    path("apis/<slug:api_id>/specificatie/<str:environment_name>", APISpecificationView.as_view(),
         name="api_specification"),
    path("organisaties/<slug:slug>", OrganizationDetailView.as_view(), name="organization_detail"),
    path("repositorys", RepositoryListView.as_view(), name="repository_list"),
    path("statistieken", StatisticsView.as_view(), name="statistics"),
    path("api-design-rules", APIDesignRulesView.as_view(), name="content_api_design_rules"),
    path("contact", ContactView.as_view(), name="contact"),
    path("contact/succes", ContactSuccessView.as_view(), name="contact_success"),
    path("over", TemplateView.as_view(title="Over Developer Overheid", template_name="content/about.html"),
         name="content_about"),
    path("privacy", TemplateView.as_view(title="Privacyverklaring", template_name="content/privacy.html"),
         name="content_privacy"),
    path("api-rules", RedirectView.as_view(pattern_name="web:content_api_design_rules", permanent=True)),
    path(".well-known/security.txt", RedirectView.as_view(url="https://www.ncsc.nl/.well-known/security.txt")),
]


if settings.DEBUG:
    urlpatterns += [
        path("error/400", default_views.bad_request, {"exception": Exception("Bad request")}),
        path("error/404", default_views.page_not_found, {"exception": Exception("Page not Found")}),
        path("error/500", default_views.server_error),
    ]
