from django.template.loader import render_to_string
from django.utils import timezone
from gitlab.v4.objects.issues import ProjectIssue
from gitlab import Gitlab, GitlabCreateError
from requests import Session
from requests.exceptions import RequestException
from requests.adapters import HTTPAdapter

from core.models.url import URL, URLProbe


HTTP_MAX_RETRIES = 2
HTTP_TIMEOUT = 5


class LinkcheckerException(Exception):
    pass


def get_urls_to_check() -> list[URL]:
    return URL.objects.all()


def create_http_client() -> Session:
    session = Session()
    adapter = HTTPAdapter(max_retries=HTTP_MAX_RETRIES)

    for prefix in "http://", "https://":
        session.mount(prefix, adapter)

    return session


def check_urls(client: Session, urls: list[URL]):
    for url in urls:
        probe = _probe_url(client, url.url)
        probe.url = url
        probe.save()


def _probe_url(client: Session, url: str) -> URLProbe:
    timestamp = timezone.now()
    status_code = None
    error = ""

    try:
        response = client.get(url, timeout=HTTP_TIMEOUT)
        status_code = response.status_code
        response.raise_for_status()
    except RequestException as e:
        error = str(e)

    return URLProbe(timestamp=timestamp, status_code=status_code, error=error)


def find_new_broken_urls(urls: list[URL], failure_threshold: int) -> list[tuple[URL, URLProbe]]:
    failed_urls = []

    for url in urls:
        probes = URLProbe.objects.filter(url=url).order_by("-timestamp")[:failure_threshold+1]
        count = probes.count()

        if count < failure_threshold:
            continue

        probes: list[URLProbe]
        for probe in probes[:failure_threshold]:
            if probe.ok():
                break
        else:
            if count > failure_threshold and not probes[failure_threshold].ok():
                break
            failed_urls.append((url, probe))

    return failed_urls


def report_urls(client: Gitlab, project_id: str, failed_urls: list[tuple[URL, URLProbe]]) -> ProjectIssue:
    context = {
        "failed_urls": [(url.url, probe.errmsg()) for url, probe in failed_urls],
    }
    description = render_to_string("issues/linkchecker_content.txt", context)

    project = client.projects.get(project_id, lazy=True)
    try:
        issue = project.issues.create({
            "title": "Broken links found",
            "description": description,
            "labels": "Broken Link",
        })
    except GitlabCreateError as e:
        raise LinkcheckerException(e) from e

    return issue
