from io import TextIOBase
from pathlib import Path
import shutil
import subprocess
import tempfile

from django.core.management.base import BaseCommand, CommandError
import yaml

from core.models.api import API, Environment, Relation, Topic
from core.models.organization import Organization
from core.models.repository import Repository
from core.models.url import URL, URLApiLink
from core.repository import RateLimitException, get_repository_updater, SourceException


class Command(BaseCommand):
    def add_arguments(self, parser):
        group = parser.add_mutually_exclusive_group(required=True)
        group.add_argument(
            '--repository',
            help='Git repository from which the content files are read',
        )
        group.add_argument(
            '--directory',
            help='Directory from which the content files are read',
        )

    def handle(self, *args, **options):
        if (directory := options['directory']) is not None:
            base_directory = Path(directory)
            if not base_directory.is_dir():
                raise CommandError(f"Directory '{base_directory}' does not exist")

        else:
            repository = options['repository']
            self.stdout.write(f'Using repository {repository}')
            checkout_directory = checkout_repository(repository)
            base_directory = Path(checkout_directory.name)

        if (directory := base_directory / 'content' / 'api').exists():
            sync_apis(directory, self.stdout)

        if (directory := base_directory / 'content' / 'repository').exists():
            sync_repositories(directory, self.stdout)

        self.stdout.write('Done')


def checkout_repository(repository: str):
    # pylint:disable=consider-using-with
    checkout_path = tempfile.TemporaryDirectory(prefix='don-sync-apis-')

    git_cmd = shutil.which('git')
    try:
        subprocess.run((git_cmd, 'clone', '--depth=1', repository, checkout_path.name), check=True)
    except subprocess.CalledProcessError:
        checkout_path.cleanup()

    return checkout_path


def sync_apis(directory: Path, stdout: TextIOBase):
    stdout.write('Syncing API\'s...')

    apis = []
    environments = []
    relations = []
    topics = []

    for file in directory.iterdir():
        with file.open() as f:
            data = yaml.safe_load(f)

        api_id = file.stem
        try:
            api, api_environments, api_relations, api_topics = parse_api(file, api_id, data)
        except Exception as e:
            raise CommandError(f"{file} is invalid: {e}") from e

        collect_api_urls(api, api_environments, stdout)

        apis.append(api)
        environments += api_environments
        relations += api_relations
        topics += api_topics

    # remove all objects from the database that do not exist in the source data
    Environment.objects.exclude(id__in=(e.id for e in environments)).delete()
    Relation.objects.exclude(id__in=(r.id for r in relations)).delete()
    API.objects.exclude(id__in=(a.id for a in apis)).delete()


def parse_api(file_name, api_id, json_data):
    """ Syncs a db API object from a json data file

    API, Environments and Relations are created or updated depending on their
    presence in the database. When updating, only data fields that are present
    in the data file are set. Other fields are not overwritten by the sync
    mechanism.

    """

    # required fields for API
    api_vals = {k: json_data.pop(k) for k in [
        "description", "service_name", "api_type", "api_authentication",
    ]}
    api_vals["organization"] = get_organization(json_data.pop("organization"))

    # optional field for API
    ref_imp_str = "is_reference_implementation"
    if ref_imp_str in json_data:
        api_vals[ref_imp_str] = json_data.pop(ref_imp_str)

    # optional fields for API from sub object in source data
    for sub_name, keys, prefix in [
            ("contact", ["email", "phone", "url"], "contact"),
            ("terms_of_use",
             ["government_only", "pay_per_use", "uptime_guarantee",
              "support_response_time"],
             "terms"),
            ("forum", ["vendor", "url"], "forum")]:
        sub_obj = json_data.pop(sub_name, {})
        api_vals.update({
            f'{prefix}_{k}': sub_obj.pop(k)
            for k in keys if k in sub_obj
        })
        if sub_obj:
            raise ValueError(
                f"Unknown key '{sub_name}.{next(iter(sub_obj))}' in API"
                " json data")

    # sync the API object with the db
    api = API.objects.update_or_create(api_id=api_id, defaults=api_vals)[0]

    # sync the environments that exist in the source data
    environments = [
        Environment.objects.update_or_create(
            api_id=api_id, name=env_data["name"], defaults={
                k: env_data.get(k, "") for k in ['api_url', 'specification_url', 'documentation_url']
            })[0]
        for env_data in json_data.pop('environments', [])
    ]

    # sync the relations that exist in the source data
    relations = [
        Relation.objects.update_or_create(
            name=relation_type,
            from_api_id=api_id,
            to_api_id=relation_api_id)[0]
        for relation_api_id, relation_types in json_data.pop(
            "relations", {}).items()
        for relation_type in relation_types
    ]

    topics = [
        topic for topic, created in (
            Topic.objects.get_or_create(name=topic_name)
            for topic_name in json_data.pop("topics", [])
        )
    ]
    api.topics.set(topics)

    # some files contain empty badges
    json_data.pop("badges", None)

    if json_data:
        # if the json is not completely stripped by now, then there is
        # unknown data. Maybe a spelling mistake.
        raise ValueError(
            f"Unknown key '{next(iter(json_data))}' in file '{file_name}'")

    return api, environments, relations, topics


def collect_api_urls(api: API, environments: list[Environment], _: TextIOBase):
    urls = {
        URLApiLink.FieldReference.CONTACT: api.contact_url,
        URLApiLink.FieldReference.FORUM: api.forum_url,
    }

    for env in environments:
        urls[URLApiLink.FieldReference(f'{env.name}_api')] = env.api_url
        urls[URLApiLink.FieldReference(f'{env.name}_spec')] = env.specification_url
        urls[URLApiLink.FieldReference(f'{env.name}_doc')] = env.documentation_url

    for field, url_str in urls.items():
        if url_str:
            url, _ = URL.objects.get_or_create(url=url_str)
            URLApiLink.objects.create(url=url, api=api, field=field)


def sync_repositories(directory: Path, stdout: TextIOBase):
    stdout.write('Syncing repositories...')

    all_repositories = []

    for file in directory.iterdir():
        with file.open() as f:
            data = yaml.safe_load(f)

        stdout.write(f"  Processing {file.name}...")
        repositories = processs_repository(data, stdout)

        all_repositories.extend(repositories)

    stdout.write('Cleaning up undefined repositories....')
    for repository in Repository.objects.exclude(url__in=(c.url for c in all_repositories)):
        stdout.write(f'  Deleting {repository.source}: {repository}...')
        repository.delete()


def processs_repository(data, stdout: TextIOBase) -> list[Repository]:
    repositories = []

    if "organization" not in data:
        stdout.write('    Missing organization, skipping file')
        return repositories

    if 'repositories' not in data:
        stdout.write('    Missing repositories, skipping file')
        return repositories

    organization = get_organization(data["organization"])
    if organization is None:
        stdout.write('    Organization does not exist, skipping file')
        return repositories

    rate_limit_hit = {}

    for repository_data in data['repositories']:
        if 'github' in repository_data:
            slug = repository_data['github']
            url = f'https://github.com/{slug}'
            source = Repository.Source.GITHUB
        elif 'gitlab' in repository_data:
            slug = repository_data['gitlab']
            url = f'https://gitlab.com/{slug}'
            source = Repository.Source.GITLAB
        else:
            stdout.write('    Unknown repository type')
            continue

        try:
            repository = Repository.objects.get(url=url)
            repository.organization = organization
        except Repository.DoesNotExist:
            if rate_limit_hit.setdefault(source, False):
                stdout.write(f'    Skipping {source}: {slug} due to rate limit')
                continue

            stdout.write(f'    Adding {source}: {slug}...')

            owner, project = slug.rsplit('/', 1)
            repository = Repository(organization=organization, owner_name=owner, name=project, source=source, url=url)

            updater = get_repository_updater(source)

            try:
                updater.update_repository(repository)
            except RateLimitException:
                rate_limit_hit[source] = True
                stdout.write('      Rate limit hit')
                continue
            except SourceException as exception:
                stdout.write(f'      Error: {exception}')
                continue

        related_apis = repository_data['apis'] if 'apis' in repository_data else []
        repository.related_apis.set(related_apis)

        repository.save()

        repositories.append(repository)

    return repositories


def get_organization(data: dict) -> Organization | None:
    try:
        organization, _ = Organization.objects.get_or_fetch(ooid=data["ooid"])
        return organization
    except Organization.DoesNotExist:
        return None
