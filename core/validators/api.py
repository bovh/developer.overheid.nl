from collections.abc import Iterable

from django.conf import settings

from core.models.api import API
from core.models.validator import ValidatorRuleset, ValidatorResult, ValidatorRule

from .base import Validator, CLIValidator


class APIMetadataValidator(Validator):
    name = "API-metagegevens validator"
    description = "Valideert of een API relevante metagegevens heeft"
    version = "1.0.0"

    @staticmethod
    def _validate_documentation(api: API, rule: ValidatorRule):
        environment = api.get_production_environment()
        passed = environment is not None and environment.documentation_url != ""
        message = "Geen documentatie URL is ingevuld voor deze API" if not passed else ""
        return ValidatorResult(passed=passed, message=message, rule=rule)

    @staticmethod
    def _validate_specification(api: API, rule: ValidatorRule):
        environment = api.get_production_environment()
        passed = environment is not None and environment.specification_url != ""
        message = "Geen specificatie URL is ingevuld voor deze API" if not passed else ""
        return ValidatorResult(passed=passed, message=message, rule=rule)

    @staticmethod
    def _validate_contact(api: API, rule: ValidatorRule):
        passed = api.contact_email != "" or api.contact_phone != "" or api.contact_url != ""
        message = "Er zijn geen contactgegevens ingevuld voor deze API" if not passed else ""
        return ValidatorResult(passed=passed, message=message, rule=rule)

    @staticmethod
    def _validate_sla(api: API, rule: ValidatorRule):
        if api.terms_support_response_time is None:
            return ValidatorResult(passed=False, message="De reactietijd voor de helpdesk is niet ingevuld", rule=rule)

        if api.terms_uptime_guarantee is None:
            return ValidatorResult(passed=False, message="De uptime-garantie is niet ingevuld", rule=rule)

        if api.terms_uptime_guarantee < (minimum_uptime := 0.9):
            return ValidatorResult(
                passed=False, message=f"De uptime-garantie moet minimaal '{minimum_uptime}' zijn", rule=rule)

        return ValidatorResult(passed=True, rule=rule)

    _rules = (
        (
            Validator.RuleMetadata(
                "documentatie",
                "Documentatie beschikbaar",
                (
                    "Het is belangrijk dat een API documentatie heeft met alle informatie die nodig is voor het "
                    "eenvoudig en effectief gebruik van de API."
                ),
            ),
            _validate_documentation
        ),
        (
            Validator.RuleMetadata(
                "specificatie",
                "Specificatie beschikbaar",
                (
                    "De API-specificatie beschrijft welke gegevens een API biedt en hoe deze gegevens kunnen worden "
                    "opgevraagd."
                ),
            ),
            _validate_specification
        ),
        (
            Validator.RuleMetadata(
                "contact",
                "Contactgegevens beschikbaar",
                (
                    "Het is belangrijk om contactgegevens te verstrekken van degenen die verantwoordelijk zijn voor "
                    "een API omdat gebruikers in contact moeten kunnen komen met iemand om problemen te rapporteren en "
                    "vragen te stellen over de API."
                ),
            ),
            _validate_contact
        ),
        (
            Validator.RuleMetadata(
                "sla",
                "Service Level Agreement (SLA) ingevuld",
                (
                    "Een Service Level Agreement (of serviceniveau-overeenkomst) is belangrijk omdat het opgeeft "
                    "wat de uptime-garantie is of er een helpdesk is, en binnen hoeveel dagen de helpdesk reageert."
                ),
            ),
            _validate_sla
        ),
    )

    def get_validator_metadata(self):
        return Validator.ValidatorMetadata(self.name, self.description, self.version)

    def get_rulesets_metadata(self):
        title = "API-metagegevens"
        description = (
            "Goede metagegevens verbetert de kwaliteit van een API. Wij controleren of belangrijke metagegevens zijn "
            "ingevuld in het API-definitiebestand: specificatie, documentatie, contactgegevens en een "
            "Service Level Agreement (SLA). Het API-definitiebestand staat op "
            "GitLab (https://gitlab.com/commonground/don/don-content) en kan door iedereen worden bijgewerkt."
        )

        def rules():
            for rule, _ in self._rules:
                yield rule

        yield Validator.RulesetMetadata("metadata", title, description, "1.0.0"), rules()

    def run(self, ruleset: ValidatorRuleset, uri: str) -> Iterable[ValidatorResult]:
        api = API.objects.get(api_id=uri)

        for (rule_id, *_), test in self._rules:
            rule = ruleset.rules.get(rule_id=rule_id)
            result = test(api, rule)
            yield result


class ADRValidator(CLIValidator):
    name = "API Design Rules validator"
    description = "Validates the compliance of an API to different modules of the API Design Rules"
    documentation_url = "https://gitlab.com/commonground/don/adr-validator"

    def __init__(self, validator_id: str):
        super().__init__(validator_id, settings.ADR_VALIDATOR_PATH)
