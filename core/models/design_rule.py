from django.db import models


TEST_VERSION_LENGTH = 200
RULE_TYPE_LENGTH = 250


class APIDesignRuleTestSuite(models.Model):
    api = models.OneToOneField(
        "core.API",
        to_field="api_id",
        on_delete=models.CASCADE,
        related_name="test_suite"
    )

    def last_design_rule_session(self):
        # first try to get the prefetched value if present, otherwise query the db
        try:
            last_session = self._last_session
        except AttributeError:
            return self.sessions.order_by("-started_at").first()

        if last_session:
            return last_session[0]

        return None

    def __str__(self):
        return f"{self.api.api_id}"


class DesignRuleSession(models.Model):
    test_suite = models.ForeignKey(
        APIDesignRuleTestSuite, on_delete=models.CASCADE, related_name="sessions"
    )
    started_at = models.DateTimeField()
    percentage_score = models.DecimalField(default=0, decimal_places=2, max_digits=5)
    test_version = models.CharField(default="", max_length=TEST_VERSION_LENGTH)

    class Meta:
        ordering = ("-started_at", )


class DesignRuleResult(models.Model):
    session = models.ForeignKey(
        DesignRuleSession, on_delete=models.CASCADE, related_name="results"
    )
    rule_type_name = models.CharField(max_length=RULE_TYPE_LENGTH, default="")
    passed = models.BooleanField(default=False)
    message = models.CharField(max_length=500, null=True)
    details = models.TextField(null=True)

    class Meta:
        ordering = ("rule_type_name", )
