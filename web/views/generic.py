from collections import OrderedDict

from django.core.paginator import Page
from django.db.models import Count, F, Q, QuerySet
from django.http import Http404, QueryDict
from django.views.generic import ListView, TemplateView as DjangoTemplateView
from django.views.generic.detail import BaseDetailView

from core.models.validator import ValidatorReportsMixin

from .mixins import PageTitleMixin, BreadcrumbMixin, Breadcrumb


class BaseView(PageTitleMixin, BreadcrumbMixin):
    pass


class TemplateView(BaseView, DjangoTemplateView):
    def get_breadcrumb(self):
        return Breadcrumb(self.title, self.request.get_full_path())


class FacetFilter:
    def __init__(self, label: str, field_name: str, choice_label_field: str, choices=None):
        """
        :choices: value,label of the choices to be displayed in the filter.
                  (default: show all values, with the value name as the label)
        """
        self.label = label
        self.field_name = field_name
        self.choice_label_field = choice_label_field
        self.choices = dict(choices) if choices is not None else {}


class FacetForm:
    def __init__(self, data, queryset: QuerySet, text_search_fields, facets_filters: dict[str: FacetFilter]):
        self.facets_filters = facets_filters
        self.url_query = QueryDict(mutable=True)

        self.search_filter = Q()
        self.search_text = data.get("q", "")
        self.topic = data.get("thema", "")

        if self.search_text != "":
            self.url_query["q"] = self.search_text

            for search_item in self.search_text.split(" "):
                if not search_item:
                    continue
                item_filter = Q()
                for field_name in text_search_fields:
                    item_filter |= Q(**{f"{field_name}__icontains": search_item})
                self.search_filter &= item_filter

        if self.topic != "":
            def query():
                q = self.url_query.copy()
                q.pop("thema")
                if not q:
                    return ""
                return f"?{q.urlencode()}"
            self.url_query["thema"] = self.topic
            self.topic_remove_query = query
            self.search_filter &= Q(topics__name__in=[self.topic])

        query_filters = OrderedDict()
        self.fields = OrderedDict()

        # Collect fields
        for field_name, facet_filter in facets_filters.items():
            field_data = data.getlist(field_name)

            self.fields[field_name] = {
                "label": facet_filter.label,
                "data": field_data,
                "options": [],
            }
            if field_data:
                self.url_query.setlist(field_name, field_data)
                query_filters[field_name] = Q(**{f"{facet_filter.field_name}__in": field_data})

        # Build options
        for field_name, facet_filter in facets_filters.items():
            other_facet_filters = [
                v for k, v in query_filters.items()
                if k != field_name and v is not None]

            combined_filter = Q(self.search_filter, *other_facet_filters)

            def build_options(facet_filter: FacetFilter, combined_filter, selected_values: list):
                def options():
                    options_qs = queryset \
                        .values(value=F(facet_filter.field_name), label=F(facet_filter.choice_label_field)) \
                        .annotate(count=Count("id", filter=combined_filter or None)) \
                        .exclude(value__isnull=True) \
                        .order_by("label")

                    if facet_filter.choices:
                        options_qs = options_qs.filter(value__in=facet_filter.choices.keys())

                    for option in options_qs:
                        if label := facet_filter.choices.get(option["value"]):
                            option["label"] = label
                        option["selected"] = str(option["value"]) in selected_values
                        yield option
                return options

            self.fields[field_name]["options"] = build_options(
                facet_filter,
                combined_filter,
                self.fields[field_name]["data"])

        self.query_filters = query_filters

    def get_search_filter(self):
        return self.search_filter & Q(*self.query_filters.values())


class FacetListView(ListView, BaseView):
    partial_template_name = ""

    paginate_by = 10
    page_kwarg = "pagina"
    pages_on_each_side = 1
    pages_on_ends = 1

    text_search_label = ""
    text_search_fields: tuple[str] = ()
    supported_facets: dict[str, str] = {}

    def get(self, request, *args, **kwargs):
        self.partial_render = request.headers.get("HX-Request", None) == "true"
        response = super().get(request, *args, **kwargs)

        if self.partial_render:
            response["Vary"] = "HX-Request"

        return response

    def get_queryset(self):
        queryset = super().get_queryset()

        self.form = FacetForm(self.request.GET, queryset, self.text_search_fields, self.facets_filters)

        return queryset.filter(self.form.get_search_filter()).distinct()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        page: Page = context["page_obj"]
        context["page_range"] = page.paginator.get_elided_page_range(
            page.number, on_each_side=self.pages_on_each_side, on_ends=self.pages_on_ends)

        context["form"] = self.form
        context["search_label"] = self.text_search_label
        context["page_kwarg"] = self.page_kwarg

        if filter_query := self.form.url_query.urlencode():
            context["filter_query"] = f"&{filter_query}"

        return context

    def get_template_names(self):
        if self.partial_render:
            return [self.partial_template_name]

        return [self.template_name]


class BaseValidatorReportsView(BaseDetailView, TemplateView):
    template_name = "validator_reports.html"
    object: ValidatorReportsMixin

    def get_context_data(self, **kwargs):
        validator_reports = self.object.latest_validator_reports

        if not validator_reports.count():
            raise Http404("No validator reports found")

        context = super().get_context_data(**kwargs)
        context["validator_reports"] = validator_reports

        return context
