const { terminalLog, sizes, config } = require("../support/e2e")

describe('About', () => {
  beforeEach(() => {
    cy.visit('/about')
  })

  context('a11y', () => {
    sizes.forEach(size => {
      it(`${size.toString().replace(",", "x")}: has no detectable a11y violations on load`, () => {
        if (Cypress._.isArray(size)) {
          cy.viewport(size[0], size[1])
        } else {
          cy.viewport(size)
        }
        cy.visit('/about')
        cy.screenshot().toMatchImageSnapshot();

        cy.injectAxe()
        // Test the page at initial load
        cy.checkA11y(null, config, terminalLog)
      })
    })
  })

})
