from typing import NamedTuple

from django.views.generic.base import ContextMixin


class PageTitleMixin(ContextMixin):
    title = ""

    def get_title(self):
        return self.title

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = self.get_title()
        return context


class Breadcrumb(NamedTuple):
    title: str
    url: str | None = None


class BreadcrumbMixin(ContextMixin):
    breadcrumb: Breadcrumb = None

    def get_breadcrumb(self) -> Breadcrumb | None:
        return self.breadcrumb

    def get_breadcrumbs(self) -> list[Breadcrumb] | None:
        if breadcrumb := self.get_breadcrumb():
            return [breadcrumb]

        return None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["breadcrumbs"] = self.get_breadcrumbs()
        return context
