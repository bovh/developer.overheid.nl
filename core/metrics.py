from collections import defaultdict
from datetime import timedelta
import logging
from random import randrange
from typing import Optional

from django.utils import timezone
from django.core.exceptions import ImproperlyConfigured
from django.db.models import Count
from django.template.loader import render_to_string
from influxdb_client import InfluxDBClient, QueryApi, Point, WritePrecision
from influxdb_client.client.flux_table import TableList
from influxdb_client.client.write_api import PointSettings
from influxdb_client.rest import ApiException as InfluxDBApiException

from don import settings
from core.models.api import API, Organization
from core.models.design_rule import DesignRuleResult, DesignRuleSession, APIDesignRuleTestSuite
from core.models.repository import Repository


WRITE_PRECISION = WritePrecision.S

logger = logging.getLogger(__name__)


def collect_metrics(influxdb_client: InfluxDBClient, bucket: str):
    points = (
        _organization_point(),
        _api_point(),
        _repository_point(),
        _api_design_rules_per_rule(),
        _api_design_rules_passed_grouped(),
    )

    with influxdb_client.write_api() as api:
        api.write(bucket=bucket, record=points, write_precision=WRITE_PRECISION)


def _organization_point() -> Point:
    return Point("organization").field("total_count", Organization.objects.count())


def _api_point() -> Point:
    count = API.objects.count()

    type_counts = {
        t["api_type"]: t["count"] for t in
        API.objects.values("api_type").annotate(count=Count("api_type")).order_by()}

    auth_counts = {
        t["api_authentication"]: t["count"] for t in
        (API.objects
            .values("api_authentication")
            .annotate(count=Count("api_authentication"))
            .order_by())
    }

    point = Point("api").field("total_count", count)

    for x in API.APIType:
        point.field(f"type_{x}_count", type_counts.get(x, 0))
    for x in API.APIAuthentication:
        point.field(f"auth_{x}_count", auth_counts.get(x, 0))

    return point


def _repository_point() -> Point:
    count = Repository.objects.count()

    source_counts = {
        t["source"]: t["count"] for t in
        Repository.objects.values("source").annotate(count=Count("source")).order_by()}

    point = Point("repository") \
        .field("total_count", count) \
        .field("source_github_count", source_counts.get(Repository.Source.GITHUB, 0)) \
        .field("source_gitlab_count", source_counts.get(Repository.Source.GITLAB, 0))

    return point


def _api_design_rules_per_rule() -> list[Point]:
    latest_test_versions = DesignRuleSession.objects \
        .distinct('test_version') \
        .order_by('test_version', '-started_at')

    versions: dict[str, dict[str, dict]] = defaultdict(lambda: defaultdict(lambda: {"total": 0, "passed": 0}))

    for test_version_session in latest_test_versions:
        sessions = DesignRuleSession.objects \
            .prefetch_related("results") \
            .distinct("test_suite") \
            .order_by("test_suite") \
            .filter(
                test_version=test_version_session.test_version,
                started_at__date=test_version_session.started_at)

        for session in sessions:
            version = session.test_version.lower().replace(" ", "_")

            for result in session.results.all():
                rule = result.rule_type_name.split(" ", 1)[0]

                versions[version][rule]["total"] += 1
                if result.passed:
                    versions[version][rule]["passed"] += 1

    return [
        (Point("design_rule")
            .tag("version", version)
            .tag("rule", rule)
            .field("total_count", counts["total"])
            .field("success_count", counts["passed"]))
        for version, rules in versions.items()
        for rule, counts in rules.items()
    ]


def _api_design_rules_passed_grouped() -> list[Point]:
    # Assume there are 7 design rules per suite.
    versions: dict[str, dict[int, int]] = defaultdict(lambda: dict.fromkeys(range(8), 0))

    suite: APIDesignRuleTestSuite
    for suite in APIDesignRuleTestSuite.objects.all():
        session = suite.last_design_rule_session()
        if session is None:
            continue

        passed = DesignRuleResult.objects \
            .filter(session=session, passed=True) \
            .aggregate(count=Count("passed"))
        passed_count = passed["count"]

        version = session.test_version.lower().replace(" ", "_")
        versions[version][passed_count] += 1

    return [
        (Point("design_rule_api_success")
            .tag("version", version)
            .tag("successes", passed_count)
            .field("apis", api_count))
        for version, buckets in versions.items()
        for passed_count, api_count in buckets.items()
    ]


def execute_query(name: str, api: QueryApi, start_days: int, window_every: int) -> TableList:
    query = render_to_string(f"influx/{name}.flux", {
        "bucket": settings.METRICS_INFLUXDB["BUCKET"],
    })
    params = {
        "start": timedelta(days=start_days),
        "stop": timezone.now(),
        "window_every": window_every,
    }

    try:
        result = api.query(query, params=params)
    except InfluxDBApiException as e:
        logger.error("Error executing query: %s", e)
        raise

    return result


def new_influxdb_client(url: Optional[str] = None, token: Optional[str] = None) -> InfluxDBClient:
    if url is None:
        url = settings.METRICS_INFLUXDB["URL"]
    if token is None:
        token = settings.METRICS_INFLUXDB["TOKEN"]

    client = InfluxDBClient(url=url, token=token, org=settings.METRICS_INFLUXDB["ORGANIZATION"])

    try:
        client.ready()
    except Exception as e:
        raise ImproperlyConfigured(f"Can't connect to InfluxDB: {e}") from e

    return client


def generate_dummy_metrics(influxdb_client: InfluxDBClient, bucket: str):
    points = []

    base = timezone.now().replace(hour=14, minute=42, second=12, microsecond=0)
    for d in range(365):
        day = base - timedelta(days=d)

        api_total_count = randrange(50, 200)

        points.extend((
            Point("organization").field("total_count", randrange(10, 50)).time(day),
            Point("api").field("total_count", api_total_count).time(day),
            Point("repository").field("total_count", randrange(100, 800)).time(day),
        ))

        points.extend(
            (Point("design_rule")
                .tag("version", "dummy")
                .tag("rule", rule)
                .field("total_count", api_total_count)
                .field("success_count", randrange(0, api_total_count))
                .time(day))
            for rule in ("DUMMY-01", "DUMMY-02", "DUMMY-03", "DUMMY-04", "DUMMY-05")
        )

        points.extend(
            (Point("design_rule_api_success")
                .tag("version", "dummy")
                .tag("successes", success_count)
                .field("apis", randrange(0, api_total_count))
                .time(day))
            for success_count in range(6)
        )

    with influxdb_client.write_api(point_settings=PointSettings(dummy="yes")) as api:
        api.write(bucket=bucket, record=points, write_precision=WRITE_PRECISION)
