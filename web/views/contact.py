from django import forms
from django.urls import reverse_lazy
from django.views.generic import FormView
import yaml

from core import gitlab

from .generic import BaseView, TemplateView
from .mixins import Breadcrumb


class ContactForm(forms.Form):
    name = forms.CharField(max_length=100, required=False)
    email_address = forms.EmailField()
    subject = forms.CharField(max_length=100)
    message = forms.CharField(widget=forms.Textarea())

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.auto_id = "field_%s"

    def create_issue(self):
        title = self.cleaned_data["subject"]
        yaml_content = yaml.dump(self.cleaned_data, sort_keys=False)
        content = f"```yaml\n{yaml_content}\n````"

        gitlab.create_issue(title, content, "Contact form")


class ContactView(FormView, BaseView):
    title = "Neem contact met ons op"
    template_name = "contact.html"
    breadcrumb = Breadcrumb("contact", reverse_lazy("web:contact"))
    success_url = reverse_lazy("web:contact_success")
    form_class = ContactForm

    def form_valid(self, form: ContactForm):
        form.create_issue()
        return super().form_valid(form)

    def get_breadcrumbs(self):
        return [
            self.breadcrumb
        ]


class ContactSuccessView(TemplateView):
    title = "Versturen gelukt"
    template_name = "contact_success.html"
    breadcrumb = Breadcrumb("succes", reverse_lazy("web:contact_success"))
    
    def get_breadcrumbs(self):
        return [
            ContactView.breadcrumb,
            self.breadcrumb
        ]
