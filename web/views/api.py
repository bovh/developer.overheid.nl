import logging

from django.core.cache import cache
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy, reverse
from django.views.generic import DetailView, FormView
import requests
import yaml

from core.models.api import API, Environment, Relation

from .generic import BaseView, FacetListView, FacetFilter, TemplateView, BaseValidatorReportsView
from .mixins import Breadcrumb
from ..forms import AddAPIForm


logger = logging.getLogger()


class APIListView(FacetListView):
    template_name = "api_list.html"
    partial_template_name = "partials/api_list_results.html"
    title = "API's"
    breadcrumb = Breadcrumb(title=title, url=reverse_lazy("web:api_list"))

    queryset = (
        API.objects
           .prefetch_related("badges", "environments", "referenced_apis", "validator_reports__report")
           .select_related("organization")
           .order_by("api_id")
    )
    text_search_label = "Vind API's"
    text_search_fields = ("service_name", "description",
                          "organization__name", "api_type",)
    facets_filters = {
        "type": FacetFilter(
            label="API type", field_name="api_type", choice_label_field="api_type", choices=API.APIType.choices),
        "organisatie": FacetFilter(
            label="Organisatie", field_name="organization", choice_label_field="organization__name"),
    }


class APIDetailView(DetailView, BaseView):
    template_name = "api_detail.html"
    slug_field = "api_id"
    slug_url_kwarg = slug_field
    queryset = API.objects.all()
    object: API

    def get_title(self):
        return self.object.service_name

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        api = self.object

        if api.is_reference_implementation:
            context["implemented_by_api_list"] = API.objects \
                .filter(relations_from__to_api=api, relations_from__name=Relation.TYPE_REFERENCE_IMPLEMENTATION) \
                .order_by("api_id")
        else:
            context["implemented_for_api_list"] = API.objects \
                .filter(relations_to__from_api=api, relations_to__name=Relation.TYPE_REFERENCE_IMPLEMENTATION) \
                .order_by("api_id")

        return context

    def get_breadcrumbs(self):
        return [
            APIListView.breadcrumb,
            self.create_breadcrumb(self.object),
        ]

    @staticmethod
    def create_breadcrumb(obj: API):
        return Breadcrumb(title=obj.service_name, url=obj.get_absolute_url())


class APISpecificationView(TemplateView):
    template_name = "api_specification.html"
    slug_field = "api_id"
    slug_url_kwarg = slug_field

    CACHE_TTL = 60 * 60

    def get_context_data(self, api_id: str, environment_name: str, **kwargs):  # pylint: disable=arguments-differ
        queryset = Environment.objects \
            .filter(name=environment_name, api_id=api_id, specification_url__isnull=False) \
            .exclude(specification_url__exact="") \
            .prefetch_related("api")
        environment = get_object_or_404(queryset)

        self.title = f"Specificatie: {environment.api.service_name}"
        self.environment = environment

        specification, specification_error = self._get_specification(api_id, environment)
        match environment.specification_url[-4:]:
            case "json":
                specification_link_suffix = " (JSON)"
            case "yaml" | ".yml":
                specification_link_suffix = " (YAML)"
            case _:
                specification_link_suffix = ""

        context = {
            "api": environment.api,
            "environment": environment,
            "specification": specification,
            "specification_error": specification_error,
            "specification_link_suffix": specification_link_suffix,
        }
        context.update(kwargs)

        return super().get_context_data(**context)

    def _get_specification(self, api_id: str, environment: Environment):
        key = f"{self.template_name}_{api_id}_{environment.name}"
        return cache.get_or_set(
            key, lambda: self._fetch_specification(environment.specification_url), self.CACHE_TTL)

    def _fetch_specification(self, url: str):
        try:
            response = requests.get(url, timeout=10, stream=True)
            if response.status_code != 200:
                return None, f"Failed to fetch specifiation from remote. Reason: {response.reason}"
        except requests.RequestException as e:
            return None, str(e)

        try:
            specification = yaml.safe_load(response.content)
        except yaml.MarkedYAMLError as e:
            error = f"{e.problem}, at line {e.problem_mark.line+1}, column {e.problem_mark.column+1}."
            return None, f"Failed to parse specification. Error: {error}"
        except yaml.YAMLError as e:
            return None, f"Failed to parse specification. Error: {e}"

        return specification, None

    def get_breadcrumbs(self):
        return [
            APIListView.breadcrumb,
            Breadcrumb(title=self.environment.api.service_name,
                       url=reverse('web:api_detail',
                                   kwargs={'api_id': self.environment.api.api_id})),
            Breadcrumb('specificatie'),
            Breadcrumb(self.environment.name, self.request.get_full_path())
        ]


class APIValidatorReportsView(BaseValidatorReportsView):
    title = "API score details"
    slug_field = "api_id"
    slug_url_kwarg = slug_field
    model = API
    object: API

    def get_breadcrumbs(self):
        return (
            APIListView.breadcrumb,
            APIDetailView.create_breadcrumb(self.object),
            super().get_breadcrumb(),
        )


class APIAddView(FormView, BaseView):
    template_name = "api_add.html"
    title = "API toevoegen"
    form_class = AddAPIForm
    success_url = reverse_lazy("web:api_add_success")
    breadcrumb = Breadcrumb("toevoegen", reverse_lazy('web:api_add'))

    def form_valid(self, form: AddAPIForm):
        form.create_issue()
        return super().form_valid(form)

    def get_breadcrumbs(self):
        return [
            APIListView.breadcrumb,
            self.breadcrumb
        ]


class APIAddSuccessView(TemplateView):
    title = "API aangeboden"
    template_name = "api_add_success.html"
    breadcrumb = Breadcrumb("succes", reverse_lazy('web:api_add_success'))

    def get_breadcrumbs(self):
        return [
            APIListView.breadcrumb,
            APIAddView.breadcrumb,
            self.breadcrumb
        ]
