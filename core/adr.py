from typing import Iterable, Tuple

from decimal import Decimal

from django.db import transaction

from core.models.api import API, Environment, APIValidatorReport
from core.models.design_rule import APIDesignRuleTestSuite, DesignRuleSession, DesignRuleResult
from core.validators.base import Validator


def get_apis_to_validate() -> Iterable[Tuple[API, Environment]]:
    apis: list[API] = API.objects.filter(api_type__in=API.ADR_TESTABLE_TYPES)

    for api in apis:
        env: Environment = api.get_production_environment()
        if not env:
            continue

        yield api, env


@transaction.atomic
def validate_api(validator: Validator, api: API, uri: str):
    report = validator.validate_ruleset("core", uri)
    APIValidatorReport.objects.create(report=report, api=api)

    test_suite, _ = APIDesignRuleTestSuite.objects.get_or_create(api=api)
    percentage_score = Decimal(100) / Decimal(report.rule_total_count) * report.rule_passed_count

    session = DesignRuleSession.objects.create(
        test_suite=test_suite,
        started_at=report.generated_at,
        percentage_score=percentage_score,
        test_version=report.ruleset.version,
    )

    for result in report.results.all():
        result_obj = DesignRuleResult(session=session, rule_type_name=result.rule.rule_id, passed=result.passed)
        if not result.passed:
            result_obj.message = result.message
            result_obj.details = result.details or None

        result_obj.save()


ADR_URL = "https://publicatie.centrumvoorstandaarden.nl/api/adr/1.0/"

DESCRIPTION_MAPPING = {
    "API-03": (
        "Only apply standard HTTP methods",
        "The HTTP specification rfc7231 and the later introduced PATCH method specification rfc5789 offer a set of"
        " standard methods, where every method is designed with explicit semantics. Adhering to the HTTP specification"
        " is crucial, since HTTP clients and middleware applications rely on standardized characteristics. Therefore,"
        " resources must be retrieved or manipulated using standard HTTP methods.",
    ),
    "API-16": (
        "Use OpenAPI Specification for documentation",
        "The OpenAPI Specification (OAS) defines a standard, language-agnostic interface to RESTful APIs which allows"
        " both humans and computers to discover and understand the capabilities of the service without access to source"
        " code, documentation, or through network traffic inspection. When properly defined, a consumer can understand"
        " and interact with the remote service with a minimal amount of implementation logic."
        "\n\n"
        "API documentation must be provided in the form of an OpenAPI definition document which conforms to the OpenAPI"
        " Specification (from v3 onwards). As a result, a variety of tools can be used to render the documentation"
        " (e.g. Swagger UI or ReDoc) or automate tasks such as testing or code generation. The OAS document should"
        " provide clear descriptions and examples.",
    ),
    "API-20": (
        "Include the major version number in the URI",
        "The URI of an API (base path) must include the major version number, prefixed by the letter v. This allows the"
        " exploration of multiple versions of an API in the browser. The minor and patch version numbers are not part"
        " of the URI and may not have any impact on existing client implementations.",
    ),
    "API-48": (
        "Leave off trailing slashes from URIs",
        "According to the URI specification rfc3986, URIs may contain a trailing slash. However, for REST APIs this is"
        " considered as a bad practice since a URI including or excluding a trailing slash might be interpreted as"
        " different resources (which is strictly speaking the correct interpretation)."
        "\n\n"
        "To avoid confusion and ambiguity, a URI must never contain a trailing slash. When requesting a resource"
        " including a trailing slash, this must result in a 404 (not found) error response and not a redirect. This"
        " enforces API consumers to use the correct URI.",
    ),
    "API-51": (
        "Publish OAS document at a standard location in JSON-format",
        "To make the OAS document easy to find and to facilitate self-discovering clients, there should be one standard"
        " location where the OAS document is available for download. Clients (such as Swagger UI or ReDoc) must be able"
        " to retrieve the document without having to authenticate. Furthermore, the CORS policy for this URI must allow"
        " external domains to read the documentation from a browser environment."
        "\n\n"
        "The standard location for the OAS document is a URI called openapi.json or openapi.yaml within the base path"
        " of the API. This can be convenient, because OAS document updates can easily become part of the CI/CD process."
        "\n\n"
        "At least the JSON format must be supported. When having multiple (major) versions of an API, every API should"
        " provide its own OAS document(s).",
    ),
    "API-56": (
        "Adhere to the Semantic Versioning model when releasing API changes",
        "Version numbering must follow the Semantic Versioning (SemVer) model to prevent breaking changes when"
        " releasing new API versions. Versions are formatted using the major.minor.patch template. When releasing a new"
        " version which contains backwards-incompatible changes, a new major version must be released. Minor and patch"
        " releases may only contain backwards compatible changes (e.g. the addition of an endpoint or an optional"
        " attribute).",
    ),
    "API-57": (
        "Return the full version number in a response header",
        "Since the URI only contains the major version, it's useful to provide the full version number in the response"
        " headers for every API call. This information could then be used for logging, debugging or auditing purposes."
        " In cases where an intermediate networking component returns an error response (e.g. a reverse proxy enforcing"
        " access policies), the version number may be omitted."
        "\n\n"
        "The version number must be returned in an HTTP response header named API-Version (case-insensitive) and should"
        " not be prefixed.",
    ),
}
