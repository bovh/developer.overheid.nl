from django.core.exceptions import ValidationError
from django.db import models, transaction
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _

from core.oo import organizations_api_client, OrganizationNotFoundError
from core.utils import slugify_max

from . import MAX_TEXT_LENGTH


class OrganizationManager(models.Manager):
    model: "Organization"

    def get_by_natural_key(self, ooid):
        return self.get(ooid=ooid)

    def get_or_fetch(self, ooid: int) -> ("Organization", bool):
        try:
            return self.get(ooid=ooid), False
        except self.model.DoesNotExist:
            with transaction.atomic(using=self.db):
                try:
                    params = self._fetch_from_remote(ooid)
                except OrganizationNotFoundError as error:
                    raise self.model.DoesNotExist from error
                return self.create(**params), True

    def fetch_and_update(self, ooid: int) -> "Organization":
        with transaction.atomic(using=self.db):
            obj: Organization = self.select_for_update().get(ooid=ooid)
            params = self._fetch_from_remote(obj.ooid)
            for name, value in params.items():
                setattr(obj, name, value)
            obj.slug = None  # Re-create slug
            obj.save(using=self.db)
            return obj

    def _fetch_from_remote(self, ooid: int) -> dict:
        data = organizations_api_client.get_organization(ooid)

        if ooid != data["systeem_id"]:
            raise ValidationError("Organization OOID mismatch")

        return {
            "ooid": ooid,
            "name": data["naam"],
            "type": types[0] if isinstance(types := data["types"], list) else None,
            "description": data["beschrijving"],
            "contact": data["contact"],
        }

    def delete_if_not_referenced(self, ooid):
        return self.filter(ooid=ooid, api__isnull=True, repository__isnull=True).delete()


class Organization(models.Model):
    ooid = models.PositiveIntegerField(_("ooid"), unique=True, help_text=_("Organisaties overheid ID"))
    name = models.CharField(_("name"), max_length=MAX_TEXT_LENGTH)
    slug = models.SlugField(max_length=100, unique=True)
    type = models.CharField(null=True)
    description = models.TextField(null=True)
    contact = models.JSONField(null=True)

    objects: OrganizationManager = OrganizationManager()

    class Meta:
        verbose_name = _("organization")

    def natural_key(self):
        return (self.ooid,)

    def get_absolute_url(self):
        return reverse("web:organization_detail", kwargs={"slug": self.slug})

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify_max(self.name)

        return super().save(*args, **kwargs)

    @cached_property
    def contact_website_urls(self):
        return self._extract_contact_values("internetadressen", "url")

    @cached_property
    def contact_email_addresses(self):
        return self._extract_contact_values("emailadressen", "email")

    @cached_property
    def contact_phone_numbers(self):
        return self._extract_contact_values("telefoonnummers", "nummer")

    def _extract_contact_values(self, field: str, key: str) -> list[str] | None:
        if not self.contact or not (items := self.contact.get(field)):
            return None

        return [value for item in items if (value := item.get(key))]
