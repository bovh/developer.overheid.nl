from collections import defaultdict
from datetime import timedelta

from django.core.exceptions import BadRequest
from django.utils import dateformat, translation
from influxdb_client.client.flux_table import FluxRecord, TableList

from core import adr

from .generic import TemplateView
from ..statistics import get_statistics, serialize_design_rules_api_successes


class StatisticsView(TemplateView):
    template_name = "statistics.html"
    title = "Statistieken"

    time_ranges = {
        "laatste-maand": (-30, timedelta(days=1)),
        "laatste-jaar": (-365, timedelta(days=30)),
    }
    default_time_range = "laatste-maand"

    def get(self, request, *args, **kwargs):
        time_range_value = request.GET.get("periode")
        if time_range_value is None:
            time_range_value = self.default_time_range
        if time_range_value not in self.time_ranges:
            raise BadRequest(f"Invalid value for 'periode' parameter: {time_range_value}")

        self.time_range = time_range_value
        self.start_days, self.window_every = self.time_ranges[time_range_value]

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        total_counts, api_design_rules_tables, design_rules_api_successes_last = (
            get_statistics(self.start_days, self.window_every))

        api_design_rules_per_rule = defaultdict(dict)
        for table in api_design_rules_tables:
            for record in table.records:
                name = record["rule"]
                api_design_rules_per_rule[name][record.get_field()] = int(record.get_value())

        def map_api_design_rules_per_rule():
            for name, fields in api_design_rules_per_rule.items():
                title, _ = adr.DESCRIPTION_MAPPING.get(name, ("", ""))
                percentage = int((fields["success_count"] / fields["total_count"]) * 100)
                yield name, title, percentage

        totals_labels, totals_series = _group_totals(total_counts)

        def get_totals(name):
            return {"labels": totals_labels[name], "series": totals_series[name]}

        context["time_range"] = self.time_range
        context["api_design_rules_per_rule"] = map_api_design_rules_per_rule
        context["graph_data"] = {
            "api-design-rules-per-score": serialize_design_rules_api_successes(design_rules_api_successes_last),
            "organization-totals": get_totals("organization"),
            "api-totals": get_totals("api"),
            "repository-totals": get_totals("repository"),
        }

        return context


def _group_totals(tables: TableList):
    labels = defaultdict(list)
    series = defaultdict(list)

    with translation.override(language="nl_NL"):
        for table in tables:
            record: FluxRecord
            for record in table.records:
                # In the frond-end the counts are displayed per day only.
                # So we're shifting the time of the metric with with 1 day and use only the date part.
                # This is tied to the Influx query because of the grouping per day.
                #
                # 2022-07-04 00:00:00+00:00 -> 2022-07-03
                date = (record.get_time() - timedelta(days=1)).date()

                labels[record.get_measurement()].append(dateformat.format(date, "j N"))
                series[record.get_measurement()].append(int(record.get_value()))

    return labels, series
