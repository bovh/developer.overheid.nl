from shutil import which
import unittest

from django.test import TestCase, override_settings

from core.models.api import API, Environment, Organization
from core.validators.api import APIMetadataValidator, ADRValidator


class APIMetadataValidatorTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.validator = APIMetadataValidator(validator_id="test")
        cls.validator.load_metadata()

        cls.organization = Organization.objects.create(ooid=1)

    def test_metadata(self):
        model = self.validator.model

        self.assertEqual(model.rulesets.count(), 1)
        self.assertEqual(model.rulesets.first().rules.count(), 4)

    def test_all_rules_passed(self):
        api = API.objects.create(
            api_id="test-api",
            organization=self.organization,
            contact_email="info@example.com",
            terms_support_response_time=5,
            terms_uptime_guarantee=99.99,
        )

        Environment.objects.create(
            name="production",
            specification_url="https://example.com/specification",
            documentation_url="https://example.com/documentation",
            api=api,
        )

        report = self.validator.validate_ruleset("metadata", "test-api")

        self.assertEqual(report.rule_total_count, 4)
        self.assertEqual(report.rule_passed_count, 4)

    def test_documentation_rule(self):
        API.objects.create(api_id="no-environment", organization=self.organization)

        report = self.validator.validate_ruleset("metadata", "no-environment")
        result = report.results.get(rule__rule_id="documentatie")
        self.assertFalse(result.passed)

        Environment.objects.create(
            name="production",
            specification_url="https://example.com/specification",
            api=API.objects.create(api_id="no-documentation", organization=self.organization),
        )

        report = self.validator.validate_ruleset("metadata", "no-documentation")
        result = report.results.get(rule__rule_id="documentatie")
        self.assertFalse(result.passed)


    def test_specification_rule(self):
        API.objects.create(api_id="no-environment", organization=self.organization)

        report = self.validator.validate_ruleset("metadata", "no-environment")
        result = report.results.get(rule__rule_id="specificatie")
        self.assertFalse(result.passed)

        Environment.objects.create(
            name="production",
            documentation_url="https://example.com/documentation",
            api=API.objects.create(api_id="no-specification", organization=self.organization),
        )

        report = self.validator.validate_ruleset("metadata", "no-specification")
        result = report.results.get(rule__rule_id="specificatie")
        self.assertFalse(result.passed)

    def test_contact_rule(self):
        API.objects.create(
            api_id="with-email",
            contact_email="info@example.com",
            organization=self.organization,
        )

        report = self.validator.validate_ruleset("metadata", "with-email")
        result = report.results.get(rule__rule_id="contact")
        self.assertTrue(result.passed)

        API.objects.create(
            api_id="with-phone",
            contact_phone="42",
            organization=self.organization,
        )

        report = self.validator.validate_ruleset("metadata", "with-phone")
        result = report.results.get(rule__rule_id="contact")
        self.assertTrue(result.passed)

        API.objects.create(
            api_id="with-url",
            contact_url="https://example.com",
            organization=self.organization,
        )

        report = self.validator.validate_ruleset("metadata", "with-url")
        result = report.results.get(rule__rule_id="contact")
        self.assertTrue(result.passed)

    def test_sla_rule(self):
        API.objects.create(
            api_id="no-support",
            terms_uptime_guarantee=99.99,
            organization=self.organization,
        )

        report = self.validator.validate_ruleset("metadata", "no-support")
        result = report.results.get(rule__rule_id="sla")
        self.assertFalse(result.passed)

        API.objects.create(
            api_id="no-uptime",
            terms_support_response_time=5,
            organization=self.organization,
        )

        report = self.validator.validate_ruleset("metadata", "no-uptime")
        result = report.results.get(rule__rule_id="sla")
        self.assertFalse(result.passed)

        API.objects.create(
            api_id="zero-uptime",
            terms_support_response_time=5,
            terms_uptime_guarantee=0,
            organization=self.organization,
        )

        report = self.validator.validate_ruleset("metadata", "zero-uptime")
        result = report.results.get(rule__rule_id="sla")
        self.assertFalse(result.passed)


ADR_VALIDATOR_PATH = which("adr-validator")


@unittest.skipUnless(ADR_VALIDATOR_PATH, "ADR-validator exectutable is required")
class ADRValidatorTests(TestCase):

    @override_settings(ADR_VALIDATOR_PATH=ADR_VALIDATOR_PATH)
    def test_metadata(self):
        validator = ADRValidator(validator_id="test")
        validator.load_metadata()

        model = validator.model

        self.assertNotEqual(model.version, "")
        self.assertNotEqual(model.documentation_url, "")

        expected_rulesets = (
            ("core", 7),
        )

        self.assertEqual(model.rulesets.count(), len(expected_rulesets))

        for ruleset_id, count in expected_rulesets:
            ruleset = model.rulesets.get(ruleset_id=ruleset_id)
            self.assertNotEqual(ruleset.ruleset_id, "")
            self.assertNotEqual(ruleset.title, "")
            self.assertNotEqual(ruleset.description, "")
            self.assertNotEqual(ruleset.version, "")
            self.assertNotEqual(ruleset.documentation_url, "")

            rules = ruleset.rules.all()
            self.assertEqual(rules.count(), count)
            for rule in rules:
                self.assertNotEqual(rule.rule_id, "")
                self.assertNotEqual(rule.title, "")
                self.assertNotEqual(rule.description, "")
                self.assertNotEqual(rule.documentation_url, "")
