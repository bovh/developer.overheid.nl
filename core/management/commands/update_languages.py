from django.core.management.base import BaseCommand, CommandError
import yaml
import requests


SOURCE = "https://raw.githubusercontent.com/github-linguist/linguist/master/lib/linguist/languages.yml"
OUTPUT = "core/languages.py"


class Command(BaseCommand):
    def handle(self, *args, **options):
        response = requests.get(SOURCE, timeout=10, stream=True)
        if not response.ok:
            raise CommandError("Vailed to download source")

        languages = yaml.safe_load(response.content)

        with open(OUTPUT, "w", encoding="utf-8") as f:
            f.write("LANGUAGES = {\n")
            for name, language in languages.items():
                if "color" in language:
                    f.write(f'    "{name}": "{language["color"]}",\n')
            f.write("}\n")
